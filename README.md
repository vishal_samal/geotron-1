This repo is for organizing the workflow for automated Grade 2 and 3 curation of GEO.

### Organization ###
The scripts for performing different steps of the workflow are glued together with [Snakemake](https://snakemake.readthedocs.io/en/stable/).

![DAG](dag.svg)

## Todo ##

### Bugs
- GEOmetalake.db has old sample tags. Take the ones from grade3_to_polly
	- Incorporate the step into pipeline
- Create additional script that transfers data from one run to GEOmetalake.db
	- Retain immutability or drop it?
- filter out non-specific only if they belong to same entity type
	- this was happening before commit 44b2777?
	- recompute
- dropna after run_bern step

- [DRY] `filter_non_specific` code is repeated in `filter_non_specific_sample_tags`
- [DRY] `assign_concept_names` code is repeated in `create_grade3_file`
- The multi-threaded implementation of `fetch_metadata` rule isn't clean
	- https://stackoverflow.com/questions/29177490/how-do-you-kill-futures-once-they-have-started
- [MED] in `tags`, replace BTO celllines with their equivalent in CVCL (Less than 1% are BTO)
	- same with celltype. or should they just be filtered out entirely?
- [MED] sample tag postprocessing scripts still implement some old deduplication hacks
- [MED] for the final tags aren't dependent on GPL, only GSE, figure out why
- [MED] renormalize diseases to only use MESH
- [MED] Automate download of conceptmapper jar?
- [MED] none of the cell lines are being tagged in `GSE13598_GPL3991`
- [LOW] figure out what to do with BTO celltypes
- [LOW] Bern PUBMED dump is a huge mess. Will need to re-run bern on GEO abstracts sometime in the future instead of using the dump.
- [VERY LOW] ignore single character entities when matching tissues/cell lines e.g. GSE90591
- [VERY LOW] encoding issues? check title of GSE150962
- [VERY LOW] apply Sqlite `CHECK` constraint on BTO and CL ontology ids maybe?
- CHEBI has cyclic relationships

### Features
- Infer `disease` tag from `cell line`
- Pick one tag per entity type at the sample level
- [HIGH] `concept_name` for Species
- [MED] Use pubmed entities also while string matching
	- Use synonyms in the ontology instead of matching just with the keyword
- [VERY LOW] Use metasra lexicon to TGFalpha/TGF-alpha issues

Done but not tested yet
- `replace(field , x'0a', ' ')` for `summary` and `overall_design`

### How to set it up ###

1. Install the packages using `conda env create -f conda-env.yml`. The name of the environment will be set to `geotron-env`.
2. `conda activate geotron-env`
3. Create a file called `data/queue/gses` and enter a few gse ids, one per line.
4. The pipeline can be run by specifying the output file you want to generate or the rule you want to run: `snakemake --cores all name_of_output_file`. Look at the `Snakefile` for names of possible output files. Example - There are 3 files I need to get sample metadata (i.e. title, source, characteristics): `snakemake --cores all data/sample_metadata/sample_{characteristics,title,source_name_ch1}`.
6. Most rules generate files containing pipe separated values. To read them using pandas use `pd.read_csv(filename, sep='|', header=None, index_col=None, names = ['column1', 'column2'])`


#### Commonly used snakemake commands

For inspecting the rules that will be executed to generate the output file: `snakemake -n output_file`

By default, snakemake only generates an output file if the dependencies for it have been recently updated. To force a it to regenerate a file use the `-R` flag: `snakemake --cores all -R output_file`
