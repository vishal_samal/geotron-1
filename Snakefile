# fetch GEO metadata using Entrez API
rule fetch_metadata:
	output:
		"data/metadata/metadata.sqlite"
	input:
		"data/queue/gses"
	script:
		"scripts/fetch_geo/fetch_metadata.py"

# querying for study-level metadata
# '\n' and '|' are replaced by space character
query_study_metadata = """
SELECT gse,
replace(replace(title, x'0a', ' '), '|', ' '),
replace(replace(summary , x'0a', ' '), '|', ' '), replace(replace(overall_design , x'0a', ' '), '|', ' ')
FROM studies
"""

rule query_study_level_metadata:
	output:
		"data/metadata/study_level_metadata"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} \"{query_study_metadata}\" > {output}"

# querying for pubmed abstracts
query_abstracts = """
SELECT t1.gse, t2.pubmed_id,
replace(replace(t2.title, x'0a', ' '), '|', ' '),
replace(replace(t2.abstract, x'0a', ' '), '|', ' ')
FROM gse_pubmed AS t1 INNER JOIN pubmed_abstracts AS t2 ON t1.pubmed_id = t2.pubmed_id
"""

rule query_pubmed_abstracts:
	output:
		"data/metadata/pubmed_abstracts"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} \"{query_abstracts}\" > {output}"

# run bern on study metadata
rule run_bern:
	output:
		"data/study_tags/bern_study_formatted",
		"data/study_tags/bern_raw_study.pickle",

		"data/study_tags/bern_pubmed_formatted",
		"data/study_tags/bern_raw_pubmed.pickle"
	input:
		"data/metadata/study_level_metadata",
		"data/metadata/pubmed_abstracts"
	script:
		"scripts/bern/call_bern_api.py"

rule renormalize_keywords:
	output:
		"data/study_tags/bern_study_processed",
		"data/study_tags/bern_pubmed_processed"
	input:
		"data/study_tags/bern_study_formatted",
		"data/study_tags/bern_pubmed_formatted",
		"data/ontologies/hgnc.tsv"
	script:
		"scripts/bern/renormalize_keywords.py"

# parsing characteristics
rule plot_characteristics_keys_histogram:
	output:
		"data/parse_characteristics_ch1/char_hist.png",
		"data/parse_characteristics_ch1/char_key_hist.csv"
	input:
		"data/sample_metadata/sample_characteristics"
	script:
		"scripts/parse_characteristics_ch1/plot_distribution.py"

query_sample_characteristics = """
SELECT t1.gse, t1.gpl, t1.gsm, replace(replace(t2.property, x'0a', ' '), '|', ' '),
replace(replace(t2.value, x'0a', ' '), '|', ' ')
FROM
	(
	SELECT gse_gsm.gse, samples.gpl, samples.gsm
	FROM gse_gsm INNER JOIN samples on gse_gsm.gsm = samples.gsm
	) as t1
INNER JOIN
sample_characteristics as t2 on t1.gsm = t2.gsm
"""

rule query_sample_characteristics:
	output:
		"data/sample_metadata/sample_characteristics"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} \"{query_sample_characteristics}\" > "
		"{output}"

query_sample_source = """
	SELECT DISTINCT gse_gsm.gse, samples.gpl, samples.gsm, samples.source_name_ch1
	FROM gse_gsm
	INNER join samples ON gse_gsm.gsm = samples.gsm
	WHERE samples.source_name_ch1 IS NOT NULL
"""
# 'dataset' refers to GSE_GPL pair
# TODO: combine 'query_sample_characteristics', 'query_sample_field', 'query_dataset_title' and 'query_sample_source' into one rule
rule query_dataset_source:
	output:
		"data/sample_metadata/dataset_source"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} '{query_sample_source}' > {output}"

rule query_dataset_title:
	output:
		"data/parse_title/dataset_title"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} '{query_sample_title}' > {output}"

# PARSE TITLE
query_sample_title = """
	SELECT DISTINCT gse_gsm.gse, samples.gpl, samples.gsm, samples.title
	FROM gse_gsm
	INNER join samples ON gse_gsm.gsm = samples.gsm
	WHERE samples.title IS NOT NULL
"""

rule parse_title:
	output:
		"data/parse_title/parsed_title_properties"
	input:
		"data/parse_title/dataset_title"
	script:
		"scripts/parse_title/extract_properties.py"

# Fetching ontologies
rule fetch_BTO_obo:
	output:
		"data/ontologies/BTO.obo"
	shell:
		"wget -O {output} 'https://raw.githubusercontent.com/BRENDA-Enzymes/BTO/master/bto.obo'"

rule fetch_CL_obo:
	output:
		"data/ontologies/CL.obo"
	shell:
		"wget -O {output} 'https://raw.githubusercontent.com/obophenotype/cell-ontology/master/cl.obo'"

rule fetch_BTO_CL_map:
	output:
		"data/ontologies/BTO_CL_map.csv"
	shell:
		"wget -O {output} 'https://www.ebi.ac.uk/spot/oxo/api/search?format=csv' --post-data 'inputSource=CL&distance=1&mappingTarget=BTO'"

rule fetch_EFO_obo:
	output:
		"data/ontologies/EFO.obo"
	shell:
		"wget -O {output} 'https://www.ebi.ac.uk/efo/efo.obo'"

rule fetch_CVCL_obo:
	output:
		"data/ontologies/CVCL.obo"
	shell:
		"wget -O - 'ftp://ftp.expasy.org/databases/cellosaurus/cellosaurus.obo' | sed 's/CVCL_/CVCL:/g' > {output}"

rule fetch_MESH:
	output:
		"data/ontologies/MESH.xml.gz"
	shell:
		"wget -O - 'ftp://nlmpubs.nlm.nih.gov/online/mesh/MESH_FILES/xmlmesh/desc2020.gz' > {output}"

rule fetch_CHEBI:
	output:
		"data/ontologies/CHEBI.obo"
	shell:
		"wget -O {output} 'http://purl.obolibrary.org/obo/chebi.obo'"

rule fetch_MESH_supplementary:
	output:
		"data/ontologies/MESH_supp.xml.gz"
	shell:
		"wget -O - 'ftp://nlmpubs.nlm.nih.gov/online/mesh/MESH_FILES/xmlmesh/supp2020.gz' > {output}"

# this url downloads only approved symbols"
rule fetch_HGNC:
	output:
		"data/ontologies/hgnc.tsv"
	shell:
		"wget -O - 'https://www.genenames.org/cgi-bin/download/custom?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=md_mim_id&status=Approved&hgnc_dbtag=on&order_by=gd_app_sym_sort&format=text&submit=submit' | tail +2 > {output}"

# Assign a type to every BTO concept - 'tissue', 'cellline', 'celltype'
# and filter plant related terms
rule refine_BTO:
	output:
		"data/ontologies/BTO_refined"
	input:
		"data/ontologies/BTO.obo",
		"data/ontologies/CVCL.obo",
	script:
		"scripts/parse_ontologies/refine_BTO.py"

# Extract disease terms only from MESH
# TODO: combined this and 'extract_MESH_supplementary'
rule extract_MESH_disease:
	output:
		temp("data/ontologies/MESH_disease_no_supplementary.json")
	input:
		"data/ontologies/MESH.xml.gz"
	script:
		"scripts/parse_ontologies/extract_mesh_disease.py"

rule extract_MESH_supplementary:
	output:
		"data/ontologies/MESH_disease.json"
	input:
		"data/ontologies/MESH_supp.xml.gz",
		"data/ontologies/MESH_disease_no_supplementary.json"
	script:
		"scripts/parse_ontologies/extract_mesh_supp.py"

rule extract_all_MESH_names:
	output:
		"data/ontologies/mesh_concept_names.psv"
	input:
		"data/ontologies/MESH.xml.gz",
		"data/ontologies/MESH_supp.xml.gz"
	script:
		"scripts/parse_ontologies/extract_all_mesh_names.py"

# Creating custom synonyms for BTO's cell lines
rule generate_cellline_lexicon:
	output:
		"data/ontologies/cellline.lexicon",
		"data/ontologies/cellline.duplicated"
	input:
		"data/ontologies/BTO_refined",
		"data/ontologies/CVCL.obo"
	script:
		"scripts/parse_ontologies/create_cellline_synonyms.py"

# Creating custom synonyms for BTO+CL tissue/celltype
rule generate_tissue_lexicon:
	output:
		"data/ontologies/tissue.lexicon",
	input:
		"data/ontologies/CL.obo",
		"data/ontologies/BTO.obo"
	script:
		"scripts/parse_ontologies/create_tissue_lexicon.py"

# Running conceptmapper on study-level metadata
cm = "data/conceptmapper"
cm_jar = "conceptmapper/conceptmapper-0.5.4.jar"
main_class = "edu.ucdenver.ccp.nlp.pipelines.conceptmapper.EntityFinder"

rule run_conceptmapper_BTO_study_level:
	output:
		"data/conceptmapper/output_BTO_formatted",
		"data/conceptmapper/cmDict-OBO.xml"
	input:
		"data/metadata/study_level_metadata",
		"data/ontologies/BTO.obo"
	shell:
		"mkdir -p {cm}/input_bto && "
		"python scripts/conceptmapper-scripts/create_input_files.py {input[0]} {cm}/input_bto && "
		"java -cp {cm_jar} {main_class} {cm}/input_bto {cm}/output_bto OBO {input[1]} {cm}/ true > /dev/null && "
		"mkdir -p {cm}/output_processed_bto &&"
		"scripts/conceptmapper-scripts/convertA1forEvaluation.pl {cm}/output_bto {cm}/output_processed_bto &&"
		"find {cm}/input_bto/ -name \"*.txt\" -print0 | xargs -0 rm && "
		"find {cm}/output_bto/ -name '*.a1' -print0 | xargs -0 rm && "
		"python scripts/conceptmapper-scripts/format_conceptmapper_output.py {cm}/output_processed_bto {output[0]} && "
		"find {cm}/output_processed_bto/ -name '*.txt' -print0 | xargs -0 rm"

rule run_conceptmapper_CL_study_level:
	output:
		"data/conceptmapper/output_CL_formatted",
		"data/conceptmapper/cmDict-CL.xml"
	input:
		"data/metadata/study_level_metadata",
		"data/ontologies/CL.obo"
	shell:
		"mkdir -p {cm}/input_cl && "
		"python scripts/conceptmapper-scripts/create_input_files.py {input[0]} {cm}/input_cl && "
		"java -cp {cm_jar} {main_class} {cm}/input_cl {cm}/output_cl CL {input[1]} {cm}/ true > /dev/null && "
		"mkdir -p {cm}/output_processed_cl &&"
		"scripts/conceptmapper-scripts/convertA1forEvaluation.pl {cm}/output_cl {cm}/output_processed_cl &&"
		"find {cm}/input_cl/ -name \"*.txt\" -print0 | xargs -0 rm && "
		"find {cm}/output_cl/ -name '*.a1' -print0 | xargs -0 rm && "
		"python scripts/conceptmapper-scripts/format_conceptmapper_output.py {cm}/output_processed_cl {output[0]} && "
		"find {cm}/output_processed_cl/ -name '*.txt' -print0 | xargs -0 rm"

# TODO: for cell ontology, remove 'cell' and 'cells'
# sed 's/cellline\/tissue/celltype/g' data/conceptmapper/output_CL_formatted | grep -v '0000000' > data/conceptmapper/output_CL_correctly_formatted

# string matching study entities with sample title & source_name_ch1

rule query_sample_field:
	output:
		"data/sample_metadata/sample_{field,(title|source_name_ch1)}"
	input:
		"data/metadata/metadata.sqlite"
	shell:
		"sqlite3 {input} 'select distinct gse_gsm.gse, samples.gsm, {wildcards.field} from gse_gsm inner join samples on gse_gsm.gsm = samples.gsm' > {output}"

rule process_conceptmapper_keywords:
	output:
		"data/study_tags/conceptmapper_study_processed"
	input:
		"data/conceptmapper/output_BTO_formatted",
		"data/ontologies/BTO_refined",
		"data/conceptmapper/output_CL_formatted"
	script:
		"scripts/post_process_tags/conceptmapper_postprocess.py"

rule filter_non_specific_study_tags:
	output:
		"data/study_tags/most_specific"
	input:
		"data/study_tags/conceptmapper_study_processed",
		"data/study_tags/bern_study_processed",
		"data/study_tags/bern_pubmed_processed",
		"data/sample_tagging/tags_processed",
		"data/sample_metadata/sample_source_name_ch1",
		"data/ontologies/BTO.obo",
		"data/ontologies/CL.obo",
		"data/ontologies/MESH_disease.json"
	script:
		"scripts/post_process_tags/most_specific_study_tags.py"

rule assign_concept_names:
	output:
		"data/study_tags/tags",
		"data/study_tags/tags_without_names"
	input:
		"data/study_tags/most_specific",
		"data/ontologies/BTO.obo",
		"data/ontologies/CL.obo",
		"data/ontologies/CVCL.obo",
		"data/ontologies/CHEBI.obo",
		"data/ontologies/mesh_concept_names.psv",
		"data/ontologies/hgnc.tsv"
	script:
		"scripts/post_process_tags/assign_concept_names.py"

rule string_match_bern_source:
	output:
		"data/sample_tagging/tags_bern_source_name_ch1"
	input:
		"data/study_tags/bern_study_processed",
		"data/sample_metadata/sample_source_name_ch1"
	script:
		"scripts/string_matching/string_matching_bern_source_name_ch1.py"

rule string_match_bern_characteristics:
	output:
		"data/sample_tagging/tags_bern_characteristics_ch1"
	input:
		"data/study_tags/bern_study_processed",
		"data/sample_metadata/sample_characteristics"
	script:
		"scripts/string_matching/string_matching_bern_characteristics_ch1.py"

rule string_match_celllines:
	input:
		"data/sample_metadata/sample_characteristics",
		"data/ontologies/cellline.lexicon"
	output:
		"data/sample_tagging/tags_cellline"
	script:
		"scripts/string_matching/celllines-match.py"

rule string_match_tissue:
	input:
		"data/sample_metadata/sample_characteristics",
		"data/sample_metadata/sample_source_name_ch1",
		"data/ontologies/tissue.lexicon",
		"data/ontologies/BTO_refined"
	output:
		"data/sample_tagging/tags_tissue"
	script:
		"scripts/string_matching/tissue-match.py"

# REGEX BASED RULES
# rule regex_extract_hour:
# 	output:
# 		"data/sample_tagging/regex_tags_characteristics"
# 	input:
# 		"data/sample_metadata/sample_characteristics"
# 	shell:
# 		"grep -iE '( |\||,)([0-9]+\.?[0-9]+|[0-9]+) ?h((r|our|ours|rs)($| |\||,|\.)|$| |\.|,)' sample_characteristics"

# cat data/sample_metadata/sample_title | grep -i 'rep' | grep --color=always -vP '[ _\/\-\|\(\.,\)\+;0-9A-Z]([bB]io|[bB]iol|[bB]iological)? ?(Rep|rep|REP)(l|lica|licate|lication|eat|icate|licat)?[- _\.#]*([0-9]+|$|[A-Za-z][0-9]?$)' | wc -l
# TAGGING CHARACTERTISTICS COLUMN

rule post_process_combine_sample_tags:
	output:
		temp("data/sample_tagging/tags_union"),
	input:
		"data/sample_tagging/tags_cellline",
		"data/sample_tagging/tags_tissue",
		"data/sample_tagging/tags_bern_source_name_ch1",
		"data/sample_tagging/tags_bern_characteristics_ch1"
	script:
		"scripts/post_process_tags/tag_deduplicate.py"

rule post_process_filter_sample_tags:
	output:
		"data/sample_tagging/tags_processed",
	input:
		"data/sample_tagging/tags_union"
	shell:
		"grep -Ev '(\|NCBI\|txid(10090|9606|10095|10116)\|)|(\|CL\|0000000\|)|(\|BTO\|0001239\|)|(\|BTO\|0005055\|)' {input} > {output}"

rule fetch_sample_tags_benchmark:
	output:
		"data/sample_tags_benchmark/tags_benchmark"
	shell:
		"polly files copy --workspace-id 6011 -s polly://sample_tags_union_latest -d {output}"

rule sample_benchmark_diff:
	output:
		"data/sample_tags_benchmark/tags_diff"
	input:
		"data/sample_tags_benchmark/tags_benchmark",
		"data/sample_tagging/tags_processed"
	shell:
		"diff -u {input[0]} {input[1]} > {output} || test $? -lt 2"

# parse extid files provided by BERN authors
rule bern_extids_disease:
	input:
		"data/bern_extids/disease_meta_190805.tsv"
	output:
		"data/bern_extids/BERN_map_disease"
	shell:
		"sed -En 's/^([0-9]+).*(MESH):([A-Z]?[0-9]+).*/\\1|\\2|\\3/p' {input}> {output} && grep -v 'MESH' {input} | sed -En 's/^([0-9]+).*(OMIM):([A-Z]?[0-9]+).*/\\1|\\2|\\3/p' >> {output}"

rule bern_extids_drug:
	input:
		"data/bern_extids/chem_meta_190806.tsv"
	output:
		"data/bern_extids/BERN_map_drug"
	shell:
		"sed -En 's/^([0-9]+).*(CHEBI|MESH):([A-Z]?[0-9]+).*/\\1|\\2|\\3/p' {input} > {output}"

rule bern_extids_gene:
	input:
		"data/bern_extids/gene_meta_190805.tsv"
	output:
		"data/bern_extids/BERN_map_gene"
	shell: # check if MIM id exists, before extracting HGNC id
		"sed -En 's/^([0-9]+).*(MIM):([0-9]+).*/\\1|\\2|\\3/p' {input}> {output} && grep -v 'MIM' {input} | sed -En 's/^([0-9]+).*(HGNC):([0-9]+).*/\\1|\\2|\\3/p' >> {output}"

rule map_bernids_to_extids:
	input:
		"data/bern_pubmed/entities_with_bernids",
		"data/bern_extids/BERN_map_gene",
		"data/bern_extids/BERN_map_drug",
		"data/bern_extids/BERN_map_disease"
	output:
		"data/bern_pubmed/entities_with_extids"
	script:
		"scripts/bern_extids/bernid_to_extid.py"

# Grouping
rule generate_column_groupings:
	input:
		"data/sample_metadata/sample_characteristics",
		"data/parse_title/parsed_title_properties",
		"data/parse_title/dataset_title",
		"data/sample_metadata/dataset_source"
	output:
		"data/grouping/column_groups",
		"data/grouping/cohorts"
	script:
		"scripts/grouping/group_column_values.py"

rule combine_column_groups:
	input:
		"data/grouping/column_groups"
	output:
		"data/grouping/combined_groups"
	script:
		"scripts/grouping/combine_groups.py"

rule fetch_sample_grouping_benchmark:
	output:
		"data/grouping/manually_curated_groupings",
	shell:
		"polly files copy --workspace-id 6011 -s polly://manually_curated_groupings -d {output}"

# Control/Perturbation labels
rule assign_control_labels:
	input:
		"data/sample_metadata/sample_characteristics",
		"data/parse_title/dataset_title",
		"data/sample_metadata/dataset_source"
	output:
		"data/grouping/control_labels"
	script:
		"scripts/grouping/assign_control_labels.py"

# Create experimental design json
rule extract_experimental_design:
	input:
		"data/sample_metadata/sample_characteristics"
	output:
		"data/experimental_design/experimental_design.json"
	script:
		"scripts/experimental_design/extract_experimental_design.py"

# There's a bigger manually curated dataset somewhere with 3k datasets
rule benchmark_groupings:
	input:
		"data/grouping/manually_curated_groupings",
		"data/grouping/column_groups"
	output:
		"data/grouping/incorrect_datasets"
	script:
		"scripts/grouping/grouping_benchmark.py"

rule filter_non_specific_sample_tags:
	output:
		"data/sample_tagging/tags_processed_specific"
	input:
		"data/sample_tagging/tags_processed",
		"data/ontologies/BTO.obo",
		"data/ontologies/CL.obo",
		"data/ontologies/MESH_disease.json"
	script:
		"scripts/post_process_tags/most_specific_sample_tags.py"

rule assign_sample_concept_names:
	input:
		"data/sample_tagging/tags_processed_specific",
		"data/ontologies/BTO.obo",
		"data/ontologies/CL.obo",
		"data/ontologies/CVCL.obo",
		"data/ontologies/CHEBI.obo",
		"data/ontologies/mesh_concept_names.psv",
		"data/ontologies/hgnc.tsv"
	output:
		"data/sample_tagging/tags_pivot"
	script:
		"scripts/post_process_tags/create_grade3_file.py"

rule insert_default_values:
	input:
		"data/sample_tagging/tags_pivot",
		"data/study_tags/tags"
	output:
		"data/sample_tagging/tags_default_values",
		"data/study_tags/tags_with_default_values.json"
	script:
		"scripts/post_process_tags/insert_default_values.py"

# Combines the 3 cohort files into one
rule collate_cohort_info:
	input:
		"data/grouping/combined_groups",
		"data/grouping/control_labels",
		"data/grouping/cohorts"
	output:
		"data/grouping/cohort_info"
	script:
		"scripts/grouping/collate_cohort_info.py"
