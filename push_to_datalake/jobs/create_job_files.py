import sys, os
import json

total_partitions = int(sys.argv[1])

job_desc = {
 "machineType" : "gp",
 "image": "docker.polly.elucidata.io/elucidata/geo_grade3_to_polly",
 "tag": "latest",
 "env": {
	 "CURRENT_PARTITION": "6",
	 "TOTAL_PARTITIONS": "10000"
 },
 "secret_env": {
	 "AWS_ACCESS_KEY_ID": os.environ["AWS_ACCESS_KEY_ID"],
	 "AWS_SECRET_ACCESS_KEY": os.environ["AWS_SECRET_ACCESS_KEY"],
	 "AWS_SESSION_TOKEN": os.environ["AWS_SESSION_TOKEN"]
 }
}

for i in range(total_partitions):
    with open(f"job_{i}.json", 'w') as f:
        job_desc['env']['CURRENT_PARTITION'] = str(i)
        job_desc['env']['TOTAL_PARTITIONS'] = str(total_partitions)
        job_desc['name'] = f"geo_curation_to_datalake_{i}"
        json.dump(job_desc, f, indent = 2)
