from cmapPy.pandasGEXpress.parse import parse
from cmapPy.pandasGEXpress.GCToo import GCToo
import pandas as pd
import cmapPy.pandasGEXpress.write_gct as wg
import boto3
import quilt3
import json
import os
s3 = boto3.client('s3')

# sample tags
df_sample_tags = pd.read_csv('tags_default_values')

# study tags
with open('tags_with_default_values.json', 'r') as f:
    study_tags = json.load(f)

# sample-level cohort info
cohort_info_df = pd.read_csv('cohort_info')

# experimental_design
with open('experimental_design.json', 'r') as f:
    design = json.load(f)

repo_path = "GEO_data_lake/data"
bucket_name = "s3://discover-prod-datalake-v1"
package = quilt3.Package.browse(repo_path, bucket_name)

dataset_id_to_key = {}
for key, entry in package.walk():
    try:
        dataset_id = entry.meta['dataset_id']
        gse, gpl = dataset_id.split('_')
    except KeyError as e:
        print("This file doesn't have valid dataset id: ", entry.meta)
        continue
    dataset_id_to_key[dataset_id] = key

del dataset_id, gse, gpl, key

def chunker_list(seq, size):
    return (seq[i::size] for i in range(size))

dataset_ids = sorted(dataset_id_to_key.keys())

# one job only modifies 1 chunk of the data
total_partitions = int(os.environ['TOTAL_PARTITIONS'])
current_partition = int(os.environ['CURRENT_PARTITION'])

assert total_partitions < len(dataset_ids)
assert current_partition >= 0 and current_partition < total_partitions

# The subset of all datasets which will be updated
datasets_to_update = list(chunker_list(dataset_ids, total_partitions))[current_partition]

# store number of samples in separate dictionary
dataset_id_to_num_samples = {}

# pull package, update GCTs, store them in folder
os.mkdir('gcts/')
os.mkdir('curated_gcts/')
for dataset_id in datasets_to_update:
    gse, gpl = dataset_id.split('_')
    key = dataset_id_to_key[dataset_id]
    filename = key.split('/')[-1]

    package[dataset_id_to_key[dataset_id]].fetch('gcts/')

    try:
        gct = parse('gcts/' + filename)
    except Exception as e:
        print("ERROR: ", dataset_id)
        print(e)
        continue

    # ignore datasets where organism is not a string
    if 'organism_ch1' not in gct.col_metadata_df.columns:
        print("organism missing: ", key)
        continue
    if gct.col_metadata_df['organism_ch1'].dtype != 'object':
        print("dee2 dataset: ", key)
        print(gct.col_metadata_df['organism_ch1'])
        continue

    col_metadata = gct.col_metadata_df.copy()
    columns_to_update = [
            col for col in \
            list(df_sample_tags.columns) + list(cohort_info_df.columns) \
            if col.startswith('kw_curated') or col.startswith('curated')
            ]

    col_metadata = col_metadata.drop(columns= columns_to_update, errors = 'ignore')

    # Merging with sample-level tags
    num_rows_before = col_metadata.shape[0]
    dataset_id_to_num_samples[dataset_id] = num_rows_before

    if 'geo_accession' not in col_metadata.columns:
        col_metadata['geo_accession'] = col_metadata.index
    try:
        col_metadata = col_metadata.reset_index().merge(
                df_sample_tags, on = ['geo_accession'], how='left'
                ).set_index('cid')
    except KeyError as e:
        print("ERROR: ", gse + '_' + gpl)
        print(e)
        continue
    # make sure no samples go missing
    assert num_rows_before == col_metadata.shape[0]

    # Merging with cohort-info df
    dataset_cohorts = cohort_info_df.loc[(cohort_info_df['gse'] == gse) & (cohort_info_df['gpl'] == gpl)][['geo_accession', 'curated_is_control', 'curated_cohort_id', 'curated_cohort_name']].drop_duplicates()
    if dataset_cohorts.shape[0] > 0:
        col_metadata = col_metadata.reset_index().merge(dataset_cohorts, on = ['geo_accession'], how='left').set_index('cid')
    try:
        assert num_rows_before == col_metadata.shape[0] # make sure no samples go missing
    except AssertionError as e:
        print("Missing samples: ", key)
        print(e)
        continue

    # make sure no samples go missing
    assert num_rows_before == col_metadata.shape[0]

    wg.write(
            GCToo(gct.data_df, gct.row_metadata_df, col_metadata),
            'curated_gcts/' + filename
            )

    os.remove('gcts/' + filename)
del num_rows_before
# pull package again, set GCTs and metadata, push

package = quilt3.Package.browse(repo_path, bucket_name)
for dataset_id in datasets_to_update:
    gse, gpl = dataset_id.split('_')
    key = dataset_id_to_key[dataset_id]
    filename = key.split('/')[-1]

    metadata = package[key].meta
    metadata['curation_version'] = 'g3.1'

    metadata['__index__']['data_required'] = 'false'

    if not os.path.exists('curated_gcts/' + filename):
        print("Skipping: ", key)
        continue

    num_samples = dataset_id_to_num_samples[dataset_id]

    # set experimental_design and total_num_samples
    # total samples in `design` should be total samples in gct
    if dataset_id not in design:
        print("experimental design not present: ", key)
    elif num_samples == design[dataset_id]['experimental_design']['total_num_samples']:
        metadata['experimental_design'] = design[dataset_id]['experimental_design']
    else:
        print('experimental design not valid: ',
                key, num_samples, design[dataset_id]['experimental_design']['total_num_samples']
                )
    metadata['total_num_samples'] = num_samples

    # TODO: Only run pipeline for those datasets for which curation has been done
    if gse not in study_tags:
        print("\nFAILURE (no study level tags): ", dataset_id)
        continue
    # update study tags
    metadata = { **metadata, **study_tags[gse] }

    # Validate?

    package.set(key, 'curated_gcts/' + filename, meta = metadata)
    print("\nSUCCESS: ", dataset_id)

package.push(repo_path, bucket_name)
