# coding: utf-8
import pandas as pd
import requests
import pickle

def query_raw(text, url="https://bern.korea.ac.kr/plain"):
    while True:
        try:
            return requests.post(url, data={'sample_text': text}, timeout=150).json()
        except (requests.Timeout, requests.ConnectionError) as e:
            print("Request timed out or couldn't connect, trying again")

def query_polly_bern(text, url="http://54.219.55.42:80"):
    while True:
        try:
            return requests.post(url, data={'text': text}, timeout=150).json()
        except (requests.Timeout, requests.ConnectionError) as e:
            print("Request timed out or couldn't connect, trying again")

def parse_response(response):
    """
    Returns a list of dictionaries.
    Each value in the list corresponds to a single entity
    """
    entities = []

    if 'denotations' not in response.keys():
        raise ValueError("Invalid response : " + str(response))

    for entity in response['denotations']:
        keyword = response['text'][entity['span']['begin']: entity['span']['end']]
        ontology = entity['id'][0].split(':')[0]
        ontology = "CUI" if ontology == "BERN" else ontology
        ontology_id = keyword if len(entity['id'][0].split(':')) == 1 else entity['id'][0].split(':')[1]

        entities.append({
            'keyword': keyword,
            'ontology': ontology,
            'ontology_id': ontology_id,
            'type': entity['obj'],
            'span_begin': entity['span']['begin'],
            'span_end': entity['span']['end'],
            })

    return entities

# Wrapping it in a function so variable names don't clash with those in 'run_on_pubmed'
def run_on_study_metadata():
    df = pd.read_csv(snakemake.input[0], sep = '|', header = None, names = ['gse', 'title', 'summary', 'overall_design'])
    print("Running bern on study metadata")
    raw_output = []
    flattened_output = []
    for i, row in df.iterrows():
        for field in ('title', 'summary', 'overall_design'):
            response = query_raw(row[field])
            try:
                entities = parse_response(response)
            except ValueError as e:
                print(row['gse'])
                # Skip this gse if there's an error
                break
            except Exception as e:
                print(row[field])
                print(row['gse'])
                print(response)
                raise e

            raw_output.append({
                'gse':row['gse'],
                'response': response,
                'field': field,
                })

            for entity in entities:
                flattened_output.append({
                    'gse': row['gse'],
                    'field': field,
                    **entity
                    })

        print("{0} of {1}".format(i, df.shape[0]))
        with open(snakemake.output[1], 'wb') as handle:
            pickle.dump(raw_output, handle)

        with open(f"{snakemake.output[1]}.backup", 'wb') as handle:
            pickle.dump(raw_output, handle)

    pd.DataFrame(flattened_output)[['gse', 'field', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end']].to_csv(snakemake.output[0], sep='|', header=None, index=None)

def run_on_pubmed():
    df_pubmed = pd.read_csv(snakemake.input[1], sep = '|', header = None, names = ['gse', 'pubmed_id', 'title', 'abstract'])
    raw_pubmed_output = []
    flattened_pubmed_output = []
    print("Running bern on pubmed")
    for i, row in df_pubmed.iterrows():
        response = query_raw(row['title'] + ' ' + row['abstract'])
        try:
            entities = parse_response(response)
        except ValueError as e:
            print(row['gse'])
            # Skip this gse if there's an error
            break

        raw_pubmed_output.append({
            'gse':row['gse'],
            'pubmed_id': row['pubmed_id'],
            'response': response,
            })

        for entity in entities:
            flattened_pubmed_output.append({
                'gse': row['gse'],
                'pubmed_id': row['pubmed_id'],
                **entity
                })

        print("{0} of {1}".format(i, df_pubmed.shape[0]))
        with open(snakemake.output[3], 'wb') as handle:
            pickle.dump(raw_pubmed_output, handle)

    pd.DataFrame(flattened_pubmed_output)[['gse', 'pubmed_id', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end']].to_csv(snakemake.output[2], sep='|', header=None, index=None)

run_on_study_metadata()
run_on_pubmed()
