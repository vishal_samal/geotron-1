import pandas as pd
import requests, json

study_cols = ['gse', 'field', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end']
pubmed_cols = ['gse', 'pubmed_id', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end']

study_df = pd.read_csv(snakemake.input[0], sep='|', names=study_cols)
study_df = study_df.dropna()

pubmed_df = pd.read_csv(snakemake.input[1], sep='|', names=pubmed_cols)
pubmed_df = pubmed_df.dropna()

hgnc_df = pd.read_csv(snakemake.input[2], sep='\t', names=['hgnc_id', 'symbol', 'long_name', 'omim_id'])
mim_to_hgnc = hgnc_df.dropna(subset=['omim_id'])
mim_to_hgnc.index = 'MIM:' + mim_to_hgnc['omim_id']
mim_to_hgnc = mim_to_hgnc.to_dict(orient='index')

cache = {}
def cached_normalize(keyword, entity_type):
    assert entity_type in ('disease', 'drug')
    if (keyword, entity_type) not in cache:
        response = requests.post(f"http://52.9.27.18:8004/{entity_type}", data=json.dumps({"text": keyword})).text
        cache[(keyword, entity_type)] = json.loads(response)
    return cache[(keyword, entity_type)]

def convert(x):
    onto_id = x['ontology'] + ':' + x['ontology_id']
    if onto_id in mim_to_hgnc:
        return mim_to_hgnc[onto_id]['hgnc_id'].split(':')
    if x['type'] == 'disease' and x['ontology'] != 'MESH':
        try:
            norm = cached_normalize(x['keyword'], x['type'])
            if norm['concept_source'] != "CUI-less":
                return [norm['concept_source'], norm['concept_id']]
        except json.JSONDecodeError as e:
            print(e)
            print(x)

    return [x['ontology'], x['ontology_id']]

print("Converting MIM to HGNC")
print("Before: ")
print(study_df['ontology'].value_counts())

study_df.loc[:, ['ontology', 'ontology_id']] = study_df[['ontology', 'ontology_id', 'keyword', 'type']].apply(convert, axis=1, result_type='expand').rename(columns={0:'ontology', 1:'ontology_id'}).copy()

pubmed_df.loc[:, ['ontology', 'ontology_id']] = pubmed_df[['ontology', 'ontology_id', 'keyword', 'type']].apply(convert, axis=1, result_type='expand').rename(columns={0:'ontology', 1:'ontology_id'}).copy()

print("After: ")
print(study_df['ontology'].value_counts())

study_df[study_cols].to_csv(snakemake.output[0], sep = '|', header = None, index=None)
pubmed_df[pubmed_cols].to_csv(snakemake.output[1], sep = '|', header = None, index=None)
