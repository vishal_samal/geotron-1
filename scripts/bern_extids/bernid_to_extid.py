import pandas as pd

print("Reading bern pubmed entities")
pubmed_entities = pd.read_csv(snakemake.input[0], sep='|', names = ['pmid', 'bern_onto', 'bern_id', 'keyword', 'type', 'begin', 'end'], dtype={'bern_id': str})

print("Reading bernid -> extid mappings")
mappings = pd.read_csv(snakemake.input[1], sep='|', names = ['bern_id', 'ontology', 'ontology_id'], dtype={'bern_id': str})

mappings = mappings.append(pd.read_csv(snakemake.input[2], sep='|', names = ['bern_id', 'ontology', 'ontology_id'], dtype={'bern_id': str}))

mappings = mappings.append(pd.read_csv(snakemake.input[3], sep='|', names = ['bern_id', 'ontology', 'ontology_id'], dtype={'bern_id': str}))

map_ = {}
for i, row in mappings.iterrows():
    map_[row['bern_id']] = [row['ontology'], row['ontology_id']]

def map_wrapper(bern_onto, bern_id, typ):
    if bern_id != bern_id:
        return [bern_onto, bern_id]
    if typ == 'species':
        return ['NCBI', 'txid'+bern_id[:-2]]
    if bern_id in map_.keys():
        return map_[bern_id]
    return [bern_onto, bern_id]

print("Mapping bernids to extids")

mapped_ids = pubmed_entities[['bern_onto', 'bern_id', 'type']].apply(lambda x: map_wrapper(x[0], x[1], x[2]), axis=1, result_type='expand')

pubmed_entities['ontology'] = mapped_ids[0]
pubmed_entities['ontology_id'] = mapped_ids[1]

print("Writing output")
pubmed_entities[['pmid', 'ontology', 'ontology_id', 'keyword', 'type', 'begin', 'end']].to_csv(snakemake.output[0], sep='|', header = None, index = None)
