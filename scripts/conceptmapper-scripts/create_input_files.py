# coding: utf-8
import pandas as pd
import sys

assert len(sys.argv) == 3

df = pd.read_csv(sys.argv[1], header = None, index_col = None, sep = '|', names = ['gse', 'title', 'summary', 'overall_design'])

df.head()
for i,row in df.iterrows():
    with open(f"{sys.argv[2]}/{row['gse']}-title.txt", 'w') as f_title:
        f_title.write(str(row['title']))
    with open(f"{sys.argv[2]}/{row['gse']}-summary.txt", 'w') as f_summary:
        f_summary.write(str(row['summary']))
    with open(f"{sys.argv[2]}/{row['gse']}-overall_design.txt", 'w') as f_design:
        f_design.write(str(row['overall_design']))

print(f"Created {i*3} input files")
