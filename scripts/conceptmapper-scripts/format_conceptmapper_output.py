import sys
import os
import pandas as pd

assert len(sys.argv) == 3
input_dir = sys.argv[1]
output_path = sys.argv[2]

files = os.listdir(input_dir)

output = []

for _file in files:
    with open(os.path.join(input_dir, _file), 'r') as f:
        lines = f.readlines()
    lines = [l[:-1] for l in lines]
    
    for line in lines:
        uri, keyword, span_begin, span_end = line.split('|')
        onto_id = uri.split('/')[-1]
        ontology, ontology_id = onto_id.split('_')
        output.append({
            "gse": _file.split('-')[0],
            "field": _file.split('-')[1].split('.')[0],
            "ontology": ontology,
            "ontology_id": ontology_id,
            "keyword": keyword,
            "span_begin": span_begin,
            "span_end": span_end
            })

pd.DataFrame(output).sort_values(by=['gse', 'field', 'span_begin']).to_csv(output_path, sep='|', header=None, index=None)
