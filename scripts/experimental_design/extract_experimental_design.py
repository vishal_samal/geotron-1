import pandas as pd
import json

df = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col=None, names=['gse', 'gpl', 'gsm', 'prop', 'value'])

print("Num datasets before filtering out nan/really long values: ", df[['gse', 'gpl']].drop_duplicates().shape[0])

# Filter out properties which have really long names or values
# Also drop nan
valid_props_df = df.dropna()[(df['prop'].str.len() < 100) & (df['value'].str.len() < 500)][['gse', 'gpl', 'prop']].drop_duplicates()

df = df.merge(valid_props_df, on = ['gse', 'gpl', 'prop'])

# Remove cases where a sample can have two prop-value pairs such that props for both are the same, e.g. 'origin' in GSE100892_GPL20828
df = df.drop_duplicates(['gse', 'gpl', 'gsm', 'prop'])

print("Num datasets after: ", df[['gse', 'gpl']].drop_duplicates().shape[0])

num_samples_df = df.groupby(['gse', 'gpl', 'prop', 'value'])['gsm'].nunique().reset_index().rename(columns={'gsm':'num_samples', 'value':'name'})

properties_df = num_samples_df.groupby(['gse', 'gpl', 'prop'])[['name', 'num_samples']].apply(lambda x: x.to_dict(orient='records')).reset_index().rename(columns={0:'properties_json'})

dataset_df = properties_df.groupby(['gse', 'gpl']).apply(lambda x: pd.Series(index=x['prop'], data=x['properties_json'].values).to_dict())

dataset_df = dataset_df.reset_index().rename(columns = {0: 'categorical_variables'})

# Find total number of samples for each dataset
total_samples_df = df
total_samples_df['dataset_id'] = total_samples_df['gse'] + '_' + total_samples_df['gpl']
total_samples_df = total_samples_df.groupby('dataset_id')['gsm'].nunique()
print("Total samples: ", total_samples_df)

output = {}
for i, row in dataset_df.iterrows():
    dataset_id = row['gse'] + '_' + row['gpl']
    # Total number of samples for this dataset
    total_num_samples = int(total_samples_df[dataset_id])

    # Making sure that `num_samples` adds up to total_num_samples for each variable
    for prop, val_list in row['categorical_variables'].items():
        # Number of samples for which a variable has non-NaN value
        num_samples = sum(val['num_samples'] for val in val_list)

        assert total_num_samples >= num_samples # Sanity check

        if total_num_samples > num_samples:
            val_list.append({
                "name": "undefined",
                "num_samples": total_num_samples - num_samples
                })

    output[dataset_id] = {
            'experimental_design': {
                'categorical_variables': row['categorical_variables'],
                'total_num_samples': total_num_samples
                }
            }

with open(snakemake.output[0], 'w') as f:
    json.dump(output, f, indent=2, sort_keys=True)
