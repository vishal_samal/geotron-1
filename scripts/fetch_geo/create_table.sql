PRAGMA foreign_keys = ON;
PRAGMA synchronous = OFF;
PRAGMA journal_mode = MEMORY;

CREATE TABLE IF NOT EXISTS studies (
	gse varchar(15) NOT NULL PRIMARY KEY,
	title text NOT NULL,
	summary text,
	overall_design text,
	type text
);

CREATE TABLE IF NOT EXISTS samples (
	gsm varchar(15) NOT NULL PRIMARY KEY,
	gpl varchar(15) NOT NULL,
	title text NOT NULL,
	source_name_ch1 text,
	organism_ch1 text NOT NULL
);

CREATE TABLE IF NOT EXISTS sample_characteristics (
	gsm varchar(15) NOT NULL,
	property text NOT NULL,
	value text,
	FOREIGN KEY(gsm) REFERENCES samples(gsm)
);

CREATE TABLE IF NOT EXISTS gse_gsm (
	gse varchar(15) NOT NULL,
	gsm varchar(15) NOT NULL,
	PRIMARY KEY (gse, gsm),
	FOREIGN KEY(gse) REFERENCES studies(gse),
	FOREIGN KEY(gsm) REFERENCES samples(gsm)
);

CREATE TABLE IF NOT EXISTS pubmed_abstracts (
	pubmed_id int NOT NULL PRIMARY KEY,
	title text NOT NULL,
	abstract text
);

CREATE TABLE IF NOT EXISTS gse_pubmed (
	gse varchar(15) NOT NULL,
	pubmed_id varchar(15) NOT NULL,
	PRIMARY KEY (gse, pubmed_id),
	FOREIGN KEY(gse) REFERENCES studies(gse),
	FOREIGN KEY(pubmed_id) REFERENCES pubmed_abstracts(pubmed_id)
);
