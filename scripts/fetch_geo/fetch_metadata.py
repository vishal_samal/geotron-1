from geo_parser import geo_entity_iterator 
import json
import xml.etree.ElementTree as ET
import urllib.request
import sqlite3
from concurrent.futures import ThreadPoolExecutor
import concurrent
import time

with open('scripts/fetch_geo/create_table.sql', 'r') as f:
    sql_script = f.read()

with sqlite3.connect(snakemake.output[0]) as start_connection:
    # conn = sqlite3.connect('file:{0}?mode=rw'.format(snakemake.output[0]), uri=True)
    start_connection.executescript(sql_script)

pubmed_url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id={0}&retmode=xml'

def innertext(tag):
    "Inner text for xml Element"
    return (tag.text or '') + ''.join(innertext(e) for e in tag) + (tag.tail or '')

def fetch_geo_metadata(accession):
    """
    Wrapper around 'geo_entity_iterator'
    """
    metadata = list(geo_entity_iterator(accession))
    assert len(metadata) == 1
    # TODO: low priority - check if decoding is happening properly
    metadata = json.loads(metadata[0].json())

    return metadata

def fetch_pubmed_abstract(pubmed_id):
    attempt = 0
    while attempt < 30:
        attempt += 1
        try:
            with urllib.request.urlopen(pubmed_url.format(pubmed_id)) as response: 
                abstract_xml = response.read()
            break
        except Exception as err:
            print("Received error from server %s" % err)
            print("Attempt %i of 30" % attempt)
            time.sleep(1 * attempt)

    root = ET.fromstring(abstract_xml)
    title = innertext(root.find('PubmedArticle/MedlineCitation/Article/ArticleTitle')).strip()
    abstract = innertext(root.find('PubmedArticle/MedlineCitation/Article/Abstract/AbstractText')).strip()

    return (title, abstract)

def store_series_metadata(metadata, conn):
    # TODO: store experiment type
    print("Storing study-level metadata")
    fields = ('accession', 'title', 'summary', 'overall_design', 'type')

    record = tuple((metadata[k] if k!='type' else metadata[k][0]) for k in fields)
    conn.execute('INSERT INTO studies VALUES (?, ?, ?, ?, ?)', record)
    conn.commit()

def store_pubmed_abstracts(metadata, conn):
    """
    Stores pubmed abstracts as well as gse -> pubmed_id mapping
    """
    print("Fetching and storing pubmed abstracts")
    for pubmed_id in metadata['pubmed_id']:
        print(f'pubmed_id: {pubmed_id}')
        title, abstract = fetch_pubmed_abstract(pubmed_id)
        try:
            conn.execute('INSERT INTO pubmed_abstracts VALUES (?, ?, ?)',
                    (pubmed_id, title, abstract)
                    )
        except sqlite3.IntegrityError as e:
            print(f"Pubmed abstract for {pubmed_id} already exists. Skipping.")
        conn.execute('INSERT INTO gse_pubmed VALUES (?, ?)',
                (metadata['accession'], pubmed_id)
                )
    conn.commit()

def extract_experiment_type(metadata):
    mappings = []
    for _type in metadata['types']:
        mappings.append({'gse':metadata['accession']})
    return mappings

def store_sample_metadata(metadata, gse, conn):
    """
    Stores sample metadata as well as gse -> gsm mappings
    """
    try:
        conn.execute('INSERT INTO samples VALUES (?, ?, ?, ?, ?)',
                (metadata['accession'], metadata['platform_id'], metadata['title'],
                    metadata['channels'][0]['source_name'], metadata['channels'][0]['organism']))
    except sqlite3.IntegrityError as e:
        print(f"Sample {metadata['accession']} already exists. \
                Only gse -> gsm mapping will be stored for this sample")
    
    # Inserting gse -> gsm mapping
    conn.execute('INSERT INTO gse_gsm VALUES (?, ?)',
            (gse, metadata['accession']))

    # Inserting sample characteristics
    properties = []
    for prop in metadata['channels'][0]['characteristics']:
        properties.append((metadata['accession'], prop['tag'], prop['value']))
    conn.executemany('INSERT INTO sample_characteristics VALUES (?, ?, ?)',
            properties)
    conn.commit()

with open(snakemake.input[0], 'r') as f:
    gses = f.readlines()
gses = [gse[:-1] for gse in gses]

def store_in_db(gse):

    # TODO: switch to snakemake.output[0]
    try:
        with sqlite3.connect(snakemake.output[0], timeout=30) as conn:

            # conn = sqlite3.connect('file:{0}?mode=rw'.format(snakemake.output[0]), uri=True)
            conn.executescript("""
                PRAGMA foreign_keys = ON;
                PRAGMA synchronous = OFF;
                PRAGMA journal_mode = MEMORY;""")
            conn.commit()

            study_metadata = fetch_geo_metadata(gse)
            store_series_metadata(study_metadata, conn)
            store_pubmed_abstracts(study_metadata, conn)

            for gsm in study_metadata['sample_id']:
                sample_metadata = fetch_geo_metadata(gsm)
                store_sample_metadata(sample_metadata, gse, conn)
    except Exception as e:
        print(e)
        raise e

# MAIN LOOP
with ThreadPoolExecutor(max_workers=100) as executor:
    future_to_gse = {executor.submit(store_in_db, gse): gse for gse in gses}

    for future in concurrent.futures.as_completed(future_to_gse):
        gse = future_to_gse[future]
        try:
            data = future.result()
        except Exception as exc:
            print('%r generated an exception: %s' % (gse, exc))
        else:
            print('%r was fetched successfuly' % (gse,))
