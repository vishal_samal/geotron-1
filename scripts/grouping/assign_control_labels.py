import pandas as pd
import requests
import json

print("Reading parsed characteristics")
df = pd.read_csv(snakemake.input[0], sep='|', names = ['gse', 'gpl', 'gsm', 'prop', 'value']).fillna('')

print("Reading title")
title_df = pd.read_csv(snakemake.input[1], sep='|', names = ['gse', 'gpl', 'gsm', 'value']).fillna('')
title_df['prop'] = 'title'

print("Reading source_name_ch1")
source_df = pd.read_csv(snakemake.input[2], sep='|', names = ['gse', 'gpl', 'gsm', 'value'])
source_df['prop'] = 'source_name_ch1'

df = df.append(title_df).append(source_df)

def sample_to_dict(table):
    """
    Creates a dictionary out of prop-value pairs
    """
    rv = {}
    for i, row in table.iterrows():
        rv[row['prop']] = row['value']
    return rv

def make_requests(dataset_df):
    """
    dataset_df needs to have 3 columns - gsm, prop, value
    """
    samples = dataset_df.groupby('gsm')[['prop', 'value']].apply(sample_to_dict)
    request_body = { 'samples': list(samples) }

    response = requests.post('http://52.9.27.18:8003/', data=json.dumps(request_body))
    response = response.json()

    del response['version']
    response['gsm'] = list(samples.index)
    return pd.DataFrame(response)

print("Calling API")
control_df = df.groupby(['gse', 'gpl'])[['gsm', 'prop', 'value']].apply(make_requests).reset_index().drop('level_2', axis=1)

print(control_df)
print(control_df['is_control'].value_counts())
control_df.to_csv(snakemake.output[0], sep='|', header=None, index=None)
