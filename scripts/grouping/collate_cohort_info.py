import pandas as pd

groups_df = pd.read_csv(snakemake.input[0], sep='|', names = ['gse', 'gpl', 'gsm', 'group_id'])

control_df = pd.read_csv(snakemake.input[1], sep='|', names=['gse', 'gpl', 'is_control', 'p_control', 'gsm'])

cohort_df = pd.read_csv(snakemake.input[2], sep='|', names=['gse', 'gpl', 'gsm', 'cohort_name'])

cohort_info_df = groups_df.merge(control_df, on = ['gse', 'gpl', 'gsm']).merge(cohort_df, on = ['gse', 'gpl', 'gsm'], how='left')[['gse', 'gpl', 'gsm', 'group_id', 'is_control', 'cohort_name']]

cohort_info_df['is_control'] = cohort_info_df['is_control'].astype(int)

print("Total samples: ", cohort_info_df.shape[0])
print(cohort_info_df['is_control'].value_counts())

cohort_info_df = cohort_info_df.rename(columns={'gsm': 'geo_accession',
    'is_control':'curated_is_control',
    'group_id':'curated_cohort_id',
    'cohort_name':'curated_cohort_name'
    })
cohort_info_df.to_csv(snakemake.output[0], index=None)
