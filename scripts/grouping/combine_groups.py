import pandas as pd

print("Reading column groups")
df = pd.read_csv(snakemake.input[0], sep = '|', header = None, index_col = None, names = ['gse', 'gpl', 'gsm', 'prop', 'group'])

df['group'] = df['group'].astype(str)

print("Combining groups")
combined_df = df.groupby(['gse', 'gpl']).apply(lambda x: x.groupby('gsm')['group'].apply(lambda y: '_'.join(y)).reset_index()).reset_index().drop(columns=['level_2'])

combined_df['group'] = combined_df.groupby(['gse', 'gpl'])['group'].transform(lambda x: x.astype('category').cat.codes)

print("Writing output")
combined_df.to_csv(snakemake.output[0], sep = '|', header = None, index = None)
