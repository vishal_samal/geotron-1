import pandas as pd
import re
import numpy as np

print("Reading parsed characteristics")
df = pd.read_csv(snakemake.input[0], sep='|', names = ['gse', 'gpl', 'gsm', 'prop', 'value']).fillna('')


# GSE123 GPL321 GSM1 tissue 'brain'
# GSE123 GPL321 GSM1 gender 'M'
# GSE123 GPL321 GSM2 tissue 'brain'
#   |
#   V
# GSE123 GPL321 GSM1 tissue 'brain'
# GSE123 GPL321 GSM1 gender 'M'
# GSE123 GPL321 GSM2 tissue 'brain'
# GSE123 GPL321 GSM2 gender ''
df_all_props = pd.merge(df[['gse', 'gpl', 'prop']].drop_duplicates(), df[['gse', 'gpl', 'gsm']].drop_duplicates(), on = ['gse', 'gpl'])
df = df_all_props.merge(df, how='left', on = ['gse', 'gpl', 'gsm', 'prop']).fillna('')


print("Reading parsed title")
df = df.append(pd.read_csv(snakemake.input[1], sep='|', names = ['gse', 'gpl', 'gsm', 'prop', 'value']).fillna(''))

print("Reading title")
title_df = pd.read_csv(snakemake.input[2], sep='|', names = ['gse', 'gpl', 'gsm', 'value']).fillna('')
title_df['prop'] = 'title'
df = df.append(title_df)

print("Reading source_name_ch1")
source_df = pd.read_csv(snakemake.input[3], sep='|', names = ['gse', 'gpl', 'gsm', 'value'])

source_df['prop'] = 'source_name_ch1'

df = df.append(source_df)

print("Datasets before grouping: ", df[['gse', 'gpl']].drop_duplicates().shape[0])

# Replace 'rep' and its variants
rep_regex = '([ _\/\-\|\(\.,\)\+;0-9A-Z])([bB]io|[bB]iol|[bB]iological)? ?(Rep|rep|REP)(l|lica|licate|lication|eat|icate|licat)?[- _\.#]*([0-9]+|$|[A-Za-z][0-9]?$)'
substitute_regex = '\\1'
df['value'] = df['value'].replace(rep_regex, substitute_regex, regex=True)
df['value'] = df['value'].replace('\d+$', '', regex=True)


df_filtered = df

# Filter out rows based on these columns
non_grouping_columns = ['([\W]|^)age([\W_]|$)', '(^|bio|[\. _#/])rep(lica|eat|\.|$)', '([\W]|^)sex([\W_]|$)', 'gender', 'batch', '(^|patient|sample|subject|donor|mouse|experiment|library|flowcell|pm|project|case|clone|plate|array|[\. _#/\(]) ?id(entif|entity|[\) \.#]|$)']

df_filtered = df_filtered[~(df_filtered['prop'].str.contains('|'.join(non_grouping_columns), flags=re.IGNORECASE, regex=True))]

# Find number of unique values for a column (per dataset)
nunique_value = df_filtered.groupby(['gse', 'gpl', 'prop'])['value'].nunique().reset_index()
nunique_gsm = df_filtered.groupby(['gse', 'gpl'])['gsm'].nunique().reset_index()
nunique = pd.merge(nunique_value, nunique_gsm, on = ['gse', 'gpl'])

# properties with more than `unique_value_cutoff` distinct value(s) withing a dataset
unique_value_cutoff = 6
valid_props = nunique[ (nunique['value'] > 1) & ( (nunique['value'] != nunique['gsm']) | (nunique['value'] < unique_value_cutoff) ) ][['gse', 'gpl', 'prop']]

df_filtered = pd.merge(df_filtered, valid_props, on = ['gse', 'gpl', 'prop'])

########################################
#----- This part creates cohorts ----- #
########################################

def clean_join(values):
    def f7(seq):
        "removes duplicates from sequence whilst preserving ordering"
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    # strip whitespaces -> filter out empty strings -> join by ';'
    return '; '.join(filter(bool, map(lambda x: x.strip(), f7(values.tolist()))))

concatenated = df_filtered.groupby(['gse', 'gpl', 'gsm']).apply(lambda x: clean_join(x['value'].astype(str)))

cohorts = concatenated.reset_index().rename(columns={0:'cohort'})

num_cohorts = cohorts.groupby(['gse', 'gpl'])['cohort'].nunique().reset_index().rename(columns={'cohort':'num_cohorts'})

# Filter out datasets which have 6 or more cohorts
cohorts_filtered = cohorts.merge(num_cohorts[num_cohorts['num_cohorts'] < 6], on=['gse', 'gpl'])[['gse', 'gpl', 'gsm', 'cohort']]

cohorts_filtered.to_csv(snakemake.output[1], header=None, sep='|', index=None)
grouped_props = df_filtered.groupby(['gse', 'gpl', 'prop'])

#######################################
#----- This part creates groups ----- #
#######################################

print("Assigning group labels")
codes = grouped_props['value'].transform(lambda x: x.astype('category').cat.codes)

df_filtered['codes'] = codes

print("Datasets with more than one group: ", df_filtered[['gse', 'gpl']].drop_duplicates().shape[0])

# To datasets where grouping isn't possible, assign the same label to all groups
df = df.merge(df_filtered, how='left')
df['codes'] = df['codes'].fillna(1).astype(int)

df[['gse', 'gpl', 'gsm', 'prop', 'codes']].to_csv(snakemake.output[0], sep='|', header = None, index = None)
