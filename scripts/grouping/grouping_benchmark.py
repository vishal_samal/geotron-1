import pandas as pd
import numpy as np

print("Reading manually curated data")
ground_truth_df = pd.read_csv(snakemake.input[0], sep = '|', index_col= None, header = None, names = ['gsm', 'group', 'gse', 'gpl'])

print("Reading model predictions")
predictions_df = pd.read_csv(snakemake.input[1], sep = '|', index_col= None, header = None, names = ['gse', 'gpl', 'gsm', 'prop', 'group'])

predictions_df = pd.merge(ground_truth_df, predictions_df, on = ['gse', 'gpl', 'gsm'])[['gsm', 'group_y', 'gse', 'gpl']]

predictions_df = predictions_df.rename(columns={'group_y': 'group'})

predictions_df['group'] = predictions_df['group'].astype(str)

predictions_combined_df = predictions_df.groupby(['gse', 'gpl']).apply(lambda x: x.groupby('gsm')['group'].apply(lambda y: '_'.join(y)).reset_index()).reset_index().drop(columns=['level_2'])

merged_df = pd.merge(ground_truth_df, predictions_combined_df, on = ['gse', 'gpl', 'gsm'])

def compare_groups(true, pred):
    hot_true = pd.get_dummies(true)
    hot_pred = pd.get_dummies(pred)
    return np.all(hot_true.dot(hot_true.T) == hot_pred.dot(hot_pred.T))

print("Comparing groupings")
output = merged_df.groupby(['gse', 'gpl']).apply(lambda x: compare_groups(x['group_x'], x['group_y']))

print(f"Accuracy: {output.sum()/len(output)}")

output.reset_index().rename(columns={0 : 'is_correct'}).to_csv(snakemake.output[0], sep='|', header = None, index = None)
