import pandas as pd
import numpy as np

df = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col=None, names=['gse', 'gpl', 'gsm', 'characteristics', 'value'])

char_counts = df.groupby('characteristics')['gse'].nunique()
sorted_char_counts = char_counts.sort_values(ascending=False)

sorted_char_counts.to_csv(snakemake.output[1], header = None, sep='|')

# How many unique GSEs, the non-top-100 are in
bottom_keys_gses = df[df['characteristics'].isin(sorted_char_counts[100:].index)]['gse'].nunique()

import matplotlib.pyplot as plt

plt.figure(figsize=(25,25))
plt.barh(np.concatenate((sorted_char_counts[:99].index, ["REST"])),
    np.concatenate((sorted_char_counts[:99].values, [bottom_keys_gses])))
plt.xlabel("Number of studies")
plt.ylabel("column name (after parsing characteristics_ch1)")
plt.savefig(snakemake.output[0])
