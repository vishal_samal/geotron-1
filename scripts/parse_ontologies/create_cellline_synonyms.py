import re
import pandas as pd
from parse_obo import parse_obo

bto_refined = pd.read_csv(snakemake.input[0], sep='|', header=None, index_col=None, names=['onto_id', 'type', 'name'], dtype=str)

output = [] # (ontology_id, synonym) pairs
for i, row in bto_refined.iterrows():
    if row['type'] != 'cellline':
        continue
    output.append({'ontology_id': row['onto_id'], 'variant': row['name']})
    if '-' in row['name']:
        output.append({'ontology_id': row['onto_id'], 'variant': row['name'].replace('-', '')})
        output.append({'ontology_id': row['onto_id'], 'variant': row['name'].replace('-', ' ')})

# read CVCL and add those names/synonyms to `output`
cl_id_to_concept, _ = parse_obo(snakemake.input[1])
for i, (ontology_id, concept) in enumerate(cl_id_to_concept.items()):
    synonyms = concept.synonyms

    output.append({ 'ontology_id': ontology_id, 'variant': concept.name })
    for syn in synonyms:
        output.append({ 'ontology_id': ontology_id, 'variant': syn })

df = pd.DataFrame(output)
df.sort_values(by = ['ontology_id'])

duplicate_counts = df.groupby('variant',as_index=False)['ontology_id'].nunique()
duplicated = duplicate_counts[duplicate_counts['ontology_id'] > 1]

print("Writing synonyms to file")
duplicated.to_csv(snakemake.output[1], header = None, index = None, sep = '|')
df.to_csv(snakemake.output[0], header = None, index = None, sep = '|')
