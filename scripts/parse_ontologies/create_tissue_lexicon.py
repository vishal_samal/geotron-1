import re
import pandas as pd
from parse_obo import parse_obo, get_nodes

output = []
cl_concepts, _ = parse_obo(snakemake.input[0])
for ontology_id, term in cl_concepts.items():

    synonyms = list(term.synonyms)

    output.append(
        {"ontology_id": ontology_id, "variant": term.name, "is_primary": True,}
    )
    for syn in synonyms:
        output.append(
            {"ontology_id": ontology_id, "variant": syn, "is_primary": False}
        )

bto_concepts, _ = parse_obo(snakemake.input[1])
# get rid of non-animal tissue/celllines
bto_subset = set(bto_concepts.values()) \
        .difference(get_nodes(bto_concepts['BTO:0000000'])) \
        .union(get_nodes(bto_concepts['BTO:0000042']))

for term in list(bto_subset):
    ontology_id, synonyms = term.ontology_id, list(term.synonyms)
    output.append(
        {"ontology_id": ontology_id, "variant": term.name, "is_primary": True}
    )
    for syn in synonyms:
        output.append(
            {"ontology_id": ontology_id, "variant": syn, "is_primary": False}
        )

# CL:0000448 white fat cell True
# CL:0000448 white fat cell False -------> CL:0000448 white fat cell True
df_syns = (
    pd.DataFrame(output)
    .sort_values(by=['ontology_id', 'is_primary'], ascending=False)
    .drop_duplicates(subset=['ontology_id', 'variant'])
)

print("\n\nNOTE: Lexicon contains duplicate synonyms, i.e. keywords that map to different ontology ids\n")
print(df_syns[df_syns.duplicated(subset='variant', keep=False)].sort_values(by='variant'))

print("\n\nTotal synonyms in lexicon: ", df_syns.shape[0])
df_syns.to_csv(snakemake.output[0], sep="|", header=None, index=None)
