# Haven't run this code yet, also it needs a lot of RAM

# Extract records and save as tsv
# Parse xml file as an ElementTree
xml_path = snakemake.input[1]
with gzip.open(xml_path) as xml_file:
    tree = xml.etree.ElementTree.parse(xml_file)
root = tree.getroot()

record_dicts = list()

for record in root:
    record_dict = dict()
    record_dict['SCRClass'] = record.get('SCRClass')
    record_dict['SupplementalRecordUI'] = record.findtext('SupplementalRecordUI')
    record_dict['SupplementalRecordName'] = record.findtext('SupplementalRecordName/String')
    record_dicts.append(record_dict)

columns = ['SupplementalRecordUI', 'SupplementalRecordName', 'SCRClass']
record_df = pandas.DataFrame(record_dicts)[columns]

supplementary_names = record_df[['SupplementalRecordUI','SupplementalRecordName']]
supplementary_names.columns = ['mesh_id', 'mesh_name']



# Read MeSH xml release
xml_path = snakemake.input[0]
with gzip.open(xml_path) as xml_file:
    tree = ET.parse(xml_file)
root = tree.getroot()

# Parse MeSH xml release
terms = list()

for elem in root:
    term = dict()
    term['mesh_id'] = elem.findtext('DescriptorUI')
    term['mesh_name'] = elem.findtext('DescriptorName/String')
    term['semantic_types'] = list({x.text for x in elem.findall(
        'ConceptList/Concept/SemanticTypeList/SemanticType/SemanticTypeUI')})
    term['synonyms'] = list({x.text for x in elem.findall('ConceptList/Concept/TermList/Term/String')})

    term['tree_numbers'] = [x.text for x in elem.findall('TreeNumberList/TreeNumber')]
    terms.append(term)

mesh_names = pandas.DataFrame.from_dict(mesh)[['mesh_id', 'mesh_name']]

mesh_names.append(supplementary_names).to_csv(snakemake.output[0], sep='|', header=None, index=None)
