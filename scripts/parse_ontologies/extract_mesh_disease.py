import os
import gzip
import json
import xml.etree.ElementTree as ET

# Read MeSH xml release
with gzip.open(snakemake.input[0]) as xml_file:
    tree = ET.parse(xml_file)
root = tree.getroot()

# Parse MeSH xml release
terms = list()

def is_human_disease(tn):
    """Given a tree number, return whether the heirarchical path suggests a human disease."""
    # F03 (mental disorders)
    if tn.startswith('F03'):
        return True
    # C01 though C21 and C24 -- C26
    for i in list(range(1, 22)) + ['C24', 'C25', 'C26']:
        if tn.startswith('C' + str(i).zfill(2)):
            return True
    # C23 exlcuding C23.888 (Symptoms and Signs)
    if tn.startswith('C23') and not tn.startswith('C23.888'):
        return True
    return False

# WARNING: If this json schema changes, then make sure to change the schema for
# the supplementary terms also
for elem in root:
    term = dict()
    term['ontology_id'] = elem.findtext('DescriptorUI')

    name = elem.findtext('DescriptorName/String')
    term['concept_name'] = name

    synonyms = {x.text for x in elem.findall('ConceptList/Concept/TermList/Term/String')}
    synonyms.discard(name)
    term['synonyms'] = list(synonyms)

    term['tree_numbers'] = [x.text for x in elem.findall('TreeNumberList/TreeNumber')]
    terms.append(term)

# Determine ontology parents
tree_number_to_id = {tn: term['ontology_id'] for term in terms for tn in term['tree_numbers']}

for term in terms:
    parents = set()
    for tree_number in term['tree_numbers']:
        try:
            parent_tn, self_tn = tree_number.rsplit('.', 1)
            parents.add(tree_number_to_id[parent_tn])
        except ValueError:
            pass
    term['parents'] = list(parents)

# Subset disease terms
disease_terms = [term for term in terms if any(map(is_human_disease, term['tree_numbers']))]

with open(snakemake.output[0], 'w') as f:
    json.dump(disease_terms, f, indent=2)
