import os
import gzip
import xml.etree.ElementTree as ET
import json

# Parse xml file as an ElementTree
xml_path = snakemake.input[0]
with gzip.open(xml_path) as xml_file:
    tree = ET.parse(xml_file)
root = tree.getroot()

# only these concepts would included in the final MESH disease dictionary
cuis_to_include = ('C000657245',)

# Extract terms and save as tsv
term_dicts = list()
for record in root.findall('SupplementalRecord'):
    if record.findtext('SupplementalRecordUI') in cuis_to_include:
        name = record.findtext('SupplementalRecordName/String')
        synonyms = {x.text for x in record.findall('ConceptList/Concept/TermList/Term/String')}
        synonyms.discard(name)
        term_dicts.append({
            'ontology_id': 'C000657245',
            'concept_name': record.findtext('SupplementalRecordName/String'),
            'synonyms': list(synonyms),
            'tree_numbers': [],
            'parents': [],
            })

with open(snakemake.input[1], 'r') as f:
    mesh_disease = json.load(f)

new_mesh_disease = mesh_disease + term_dicts

with open(snakemake.output[0], 'w') as f:
    json.dump(new_mesh_disease, f, indent = 2)
