from parse_obo import Concept
import json

def parse_mesh_json(filepath):
    with open(filepath, 'r') as f:
        mesh = json.load(f)
    
    concepts, roots = {}, set()
    # Populate the `concepts` dictionary
    for term in mesh:
        ontology_id = 'MESH:' + term['ontology_id']
        concepts[ontology_id] = Concept(ontology_id = ontology_id,
                name = term['concept_name'],
                synonyms = term['synonyms'])

        if not term['parents']:
            roots.add(concepts[ontology_id])

    # add descendants to the Concept objects in the dictionary
    for term in mesh:
        num_parents = len(term['parents'])
        # all relationships seem to be 'is_a' relationships
        for parent_oid in term['parents']:
            try:
                concepts['MESH:'+parent_oid].descendants.add(
                    ('is_a', concepts['MESH:'+term['ontology_id']])
                    )
            except KeyError as e:
                # Its possible that a concept's parents are not present in the
                # json. If it has more than one parent then we can let it pass
                if num_parents < 1:
                    raise e

    return concepts, roots


if __name__ == "__main__":
    concepts, roots = parse_mesh_json('data/ontologies/MESH_disease.json')
    import pdb; pdb.set_trace()
