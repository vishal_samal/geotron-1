# NOTE: This script has only been tested with BTO, CL and CVCL

import pandas as pd
import re

class Concept():
    """
    Represents a node in the ontology tree. It points to child nodes.
    This is a hashable object and its hash is determined by the ontology_id
    """
    def __init__(self, ontology_id, name, synonyms = set(), descendants = [], xrefs = set()):
        assert ':' in ontology_id and len(ontology_id.split(':')) == 2
        self.ontology_id = ontology_id
        self.name = name
        self.synonyms = set(synonyms)
        self.descendants = set(descendants)
        self.xrefs = set(xrefs)
    def __eq__(self, other):
        return self.ontology_id == other.ontology_id
    def __hash__(self):
        return hash(self.ontology_id)
    def __repr__(self):
        return self.ontology_id + ' - ' + \
        self.name + f" <synonyms: {self.synonyms}> <num_direct_descendants: " + str(len(self.descendants)) + "> "

def parse_obo(filepath, is_chebi=False):
    """
    Parses an obo file and returns:
    1. A dictionary mapping an ontology_id to a Concept object
    2. A list of root nodes (Concepts) in the ontology. There can be more than
    one root.
    """
    if 'chebi' in filepath.lower() and not is_chebi:
        raise Exception("Set is_chebi as True when using CHEBI")

    # most of the weird symbols in concept names are present in CVCL
    name_pattern = "[\w,' /:\.\+\(\)\[\]" + "′\"±}{><=$?\\\–#*>^;&~-]+"

    onto_pattern = '[A-Z]+:[A-Z0-9]+'
    name_exp = re.compile(f"\nname: ({name_pattern})\n")
    onto_exp = re.compile(f"\nid: ({onto_pattern})\n")
    synonym_exp = re.compile(f'\nsynonym: "({name_pattern})" ')
    is_a_exp = re.compile(f"\nis_a: ({onto_pattern}) !")
    if is_chebi:
        is_a_exp = re.compile(f"\nis_a: ({onto_pattern})\n")
    relationship_exp = re.compile(f"\nrelationship: ([a-z]+_[a-z]+) ({onto_pattern}) !")
    is_obsolete_exp = re.compile("\nis_obsolete: true")

    # NOTE: CVCL writes xrefs for BTO in this manner:
    # xref: BTO:BTO:0000042
    xref_exp = re.compile(f"xref: (.+)\n")

    with open(filepath, 'r') as f:
        lines = f.read()

    sections = lines.split('\n\n')
    all_terms = list(filter(lambda x: x[:6] == '[Term]', sections))

    terms = list(filter(lambda x: not re.search(is_obsolete_exp, x), all_terms))

    concepts = {}
    # add Concept objects to the dictionary
    for i, term in enumerate(terms):
        try:
            oid = re.search(onto_exp, term).group(1)
            name = re.search(name_exp, term).group(1)
        except AttributeError as e:
            print(term)
            raise e
        syns = re.findall(synonym_exp, term)
        xrefs = re.findall(xref_exp, term)
        concepts[oid] = Concept(oid, name, syns, xrefs=xrefs)

    # add descendants to the Concept objects in the dictionary
    for i, term in enumerate(terms):
        child_oid = re.search(onto_exp, term).group(1)

        # all 'is_a' relationships
        for parent_oid in re.findall(is_a_exp, term):
            concepts[parent_oid].descendants.add(('is_a', concepts[child_oid]))
        # all other relationships
        for relation, parent_oid in re.findall(relationship_exp, term):
            concepts[parent_oid].descendants.add((relation, concepts[child_oid]))

    # create a list of roots
    all_children = set()
    for concept in concepts.values():
        all_children = all_children.union(get_nodes(concept).difference(set([concept])))

    roots = set(concepts.values()).difference(all_children)

    return concepts, roots

def get_nodes(root):
    """
    Given a Concept object, return the Set of all direct and indirect
    descendants and the root itself.
    """
    nodes = set([root])
    for _, child in root.descendants:
        nodes = nodes.union(get_nodes(child))
    return nodes

def get_lexicon(concepts):
    """
    Given a concept dict, return all the name/synonym -> onto_id mappings
    Returns a dataframe
    """
    mappings = []
    for onto_id, concept in concepts.items():
        mappings.append({
            "synonyms": concept.name,
            "onto_id" : onto_id,
            "name": concept.name,
            })
        for syn in concept.synonyms:
            mappings.append({
                "synonyms": syn,
                "onto_id" : onto_id,
                "name": concept.name,
                })
    return pd.DataFrame(mappings)


if __name__ == "__main__":
    # Testing
    chebi, chebi_roots = parse_obo('data/ontologies/CHEBI.obo', is_chebi=True)
    print("Number of concepts in CHEBI: ", len(chebi))
    print("Number of roots in CHEBI: ", len(chebi_roots))
    print("Haven't checked external references for CHEBI")

    cl, cl_roots = parse_obo('data/ontologies/CL.obo')
    assert set(['CALOHA:TS-2035', 'FMA:68646', 'GO:0005623', 'KUPO:0000002', 'WBbt:0004017', 'VHOG:0001533']) == cl['CL:0000000'].xrefs
    print("External references for CL:0000000 - ", cl['CL:0000000'].xrefs)

    assert set(['BTO:0002290']) == cl['CL:0000001'].xrefs
    print("External references for CL:0000001 - ", cl['CL:0000001'].xrefs)

    assert len(cl) == 2248
    assert len(cl_roots) == 1
    print("Number of concepts in CL: ", len(cl))
    print("Number of roots in CL: ", len(cl_roots))

    bto, bto_roots = parse_obo('data/ontologies/BTO.obo')
    assert len(bto) == 6378
    assert len(bto_roots) == 22
    print("Number of concepts in BTO: ", len(bto))
    print("Number of roots in BTO: ", len(bto_roots))

    cvcl, cvcl_roots = parse_obo('data/ontologies/CVCL.obo')
    assert len(cvcl) == 124601
    assert len(cvcl_roots) == 85498
    print("Number of concepts in CVCL: ", len(cvcl))
    print("Number of roots in CVCL: ", len(cvcl_roots))
