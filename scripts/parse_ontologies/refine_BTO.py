import pandas as pd
import re
from parse_obo import parse_obo, get_nodes

print("Reading BTO")
bto_concepts, _ = parse_obo(snakemake.input[0])

print("Reading CVCL")
cvcl_concepts, _ = parse_obo(snakemake.input[1])

# Any BTO ids found as xrefs in CVCL are put into this set
bto_celllines = set()
for _, concept in cvcl_concepts.items():
    for xref in concept.xrefs:
        if xref[:3] == 'BTO':
            bto_celllines.add(xref[4:])

def is_cellline(concept):
    return ('cell line' in concept.name) or (concept.ontology_id in bto_celllines)

def is_celltype(concept):
    if ('cell' in concept.name) or ('cyte' in concept.name):
        return True
    if len(concept.name) >= 5 and (concept.name[-5:] == 'blast'):
        return True
    for syn in concept.synonyms:
        if ('cell' in syn) or ('cyte' in syn):
            return True
        if len(syn) >= 5 and (syn[-5:] == 'blast'):
            return True
    return False

# { 'BTO:0000042': 'tissue' , ... }
bto_types = { onto_id: ('celltype' if is_celltype(concept) else 'tissue')
       for onto_id, concept in bto_concepts.items() }

def assign_descendant_type(parent_id, _type):
    """
    Assigns the type "_type" to this BTO id and to all its descendants
    """
    # Some CVCL xrefs seem to include obsolete terms (e.g. BTO:0004004)
    if parent_id not in bto_concepts.keys():
        return

    bto_types[parent_id] = _type

    for _, child in bto_concepts[parent_id].descendants:
        assign_descendant_type(child.ontology_id, _type)

# A concept will be a cell line in BTO if its parent is also a cell line
for _, concept in bto_concepts.items():
    if is_cellline(concept):
        assign_descendant_type(concept.ontology_id, 'cellline')

df = pd.DataFrame(bto_types.items())
df.columns = ['ontology_id', 'type']
df['name'] = df['ontology_id'].map(lambda o: bto_concepts[o].name)

# get rid of non-animal tissue/celllines
bto_subset = set(bto_concepts.values()) \
        .difference(get_nodes(bto_concepts['BTO:0000000'])) \
        .union(get_nodes(bto_concepts['BTO:0000042'])) \
        .difference([bto_concepts['BTO:0000042']])

bto_subset_id = set([concept.ontology_id for concept in bto_subset])

df = df.loc[df['ontology_id'].isin(bto_subset_id), :] 
print("Total: ", df.shape[0])
print("Celllines: ", df[df['type'] == 'cellline'].shape[0])
print("Celltypes: ", df[df['type'] == 'celltype'].shape[0])
print("Tissues: ", df[df['type'] == 'tissue'].shape[0])
print("Discarded: ", len(bto_concepts) - len(bto_subset))

df.to_csv(snakemake.output[0], sep="|", header=None, index=None)
