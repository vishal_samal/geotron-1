import pandas as pd
import re
import numpy as np

print("Reading input files")
df = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col=None, names=['gse', 'gpl', 'gsm', 'title'])
df['title'] = df['title'].fillna('')

def get_ntime(s_orig):
    try:
        s_list = re.split(" |_|-|,", s_orig.lower())
        time_str_list = []
        # n - time
        for s in s_list:
            a = re.search("^\d+((\.\d+)?)(h|hr|hours|hrs|wk|day|d|days|month|months|week|wks|min)$",s)
            if a:
                time_str_list.append(s[a.start():a.end()])
        # time -n 
        for s in s_list:
            # remove h from here because hn occurs in some datasets
            a = re.search("^(hr|hours|hrs|wk|day|d|days|month|months|week|wks|min)\d+((\.\d+)?)$",s)
            if a:
                time_str_list.append(s[a.start():a.end()]) 
        
        if len(time_str_list)!=0:
            return " ".join(time_str_list)
        else:
            time_units = "h|hr|hours|hrs|wk|day|d|days|month|months|week|wks|min".split("|")
            s_list2 = re.split(r'[`\-=~!@#$%^&*()_+\[\]{};\'\\:"|<,./<>? ]', s_orig.lower())
            if not any([w in time_units for w in s_list2]):
                return "NA"
            
            time_w_index = np.argmax([w in time_units for w in s_list2])
            
            if time_w_index == 0:
                if s_list2[time_w_index + 1].replace(".", "").isdigit() and len(s_list2[time_w_index + 1]) < 3:
                    return " ".join(s_list2[(time_w_index):time_w_index+2])
                else:
                    return "NA"
            if time_w_index == (len(s_list2)-1):
                if s_list2[time_w_index - 1].replace(".", "").isdigit() and len(s_list2[time_w_index - 1])< 3:
                    return(" ".join(s_list2[(time_w_index-1):time_w_index+1]))
                else:
                    return "NA"

            if s_list2[time_w_index - 1].replace(".", "").isdigit() and len(s_list2[time_w_index - 1])< 3:
                return(" ".join(s_list2[(time_w_index-1):time_w_index+1]))
            elif s_list2[time_w_index + 1].replace(".", "").isdigit() and len(s_list2[time_w_index + 1])< 3:
                return(" ".join(s_list2[(time_w_index):time_w_index+2]))
        
        return "NA"

    except IndexError as e:
        print("Index Error when input is: ", s_orig)
        return "NA"

def get_sirna(s_orig):
    s_list = re.split(" |_|-|,", s_orig.lower())
    sgene_list = []
    # si/shgene
    for s in s_list:
        a = re.search("^(si|sh)[a-z]+",s)
        if a:
            sgene_list.append(s[a.start():a.end()])
    
    if len(sgene_list)!=0:
        return " ".join(sgene_list)
    
    return "NA"

def get_drug_dose(s_orig):
    s_list2 = re.split(" |_|-|,", s_orig.lower())
    s_dose_list = []
    for s in s_list2:
        a = re.search("^\d+((\.\d+)?)(ug/ml|nm|μm|ng/ml|nug|nμg|ug)$",s)
        if a:
            s_dose_list.append(s[a.start():a.end()])
    
    if len(s_dose_list)!=0:
        return " ".join(s_dose_list)
        
    if len(s_dose_list) == 0:
        dose_units = "ug/ml|nm".split("|")
        if not any([w in dose_units for w in s_list2]):
            return "NA"
        
        dose_unit_index = np.argmax([w in dose_units for w in s_list2])
        
        if dose_unit_index == (len(s_list2)-1):
            if s_list2[dose_unit_index - 1].replace(".", "").isdigit():
                return(" ".join(s_list2[(dose_unit_index-1):dose_unit_index+1]))
            else:
                return "NA"
        
        if s_list2[dose_unit_index - 1].replace(".", "").isdigit():
            return(" ".join(s_list2[(dose_unit_index-1):dose_unit_index+1]))
        
    return "NA"

print("Extracting drug")
drug_dosage_df = df[['gse', 'gpl', 'gsm']].copy()
drug_dosage_df['property'] = 'drug_dosage_t'
drug_dosage_df['value'] = df['title'].apply(get_drug_dose).replace("NA", np.nan)
drug_dosage_df.dropna(inplace = True)

print("Extracting time")
time_df = df[['gse', 'gpl', 'gsm']].copy()
time_df['property'] = 'time_t'
time_df['value'] = df['title'].apply(get_ntime).replace("NA", np.nan)
time_df.dropna(inplace = True)

print("Extracting sirna")
sirna_df = df[['gse', 'gpl', 'gsm']].copy()
sirna_df['property'] = 'sirna_t'
sirna_df['value'] = df['title'].apply(get_sirna).replace("NA", np.nan)
sirna_df.dropna(inplace = True)

drug_dosage_df.append(time_df).append(sirna_df).to_csv(snakemake.output[0], sep = '|', header = None, index = None)
