import pandas as pd
import sys, os
sys.path.append(os.path.join(os.getcwd(), 'scripts/parse_ontologies/'))
from parse_obo import parse_obo

df_tags = pd.read_csv(snakemake.input[0], sep='|', header=None, index_col=None, names=['gse', 'type', 'ontology', 'ontology_id'])


df_tags['onto_id'] = df_tags['ontology'] + ':' + df_tags['ontology_id']

print("Loading ontologies...")

bto, bto_roots = parse_obo(snakemake.input[1])
cl, cl_roots = parse_obo(snakemake.input[2])
cvcl, cvcl_roots = parse_obo(snakemake.input[3])
chebi, chebi_roots = parse_obo(snakemake.input[4], is_chebi=True)

# to remove instances of 'atom' from CHEBI
atom_id = 'CHEBI:33250'
queue = [atom_id]
name_of_atoms = set([])

while(len(queue) != 0):
    current_node = queue.pop(0)
    queue += [i[1].ontology_id for i in list(chebi[current_node].descendants)]
    name_of_atoms.add(chebi[current_node].ontology_id)

for i in name_of_atoms:
    _ = chebi.pop(i)

hgnc = pd.read_csv(snakemake.input[6], sep='\t', names=['symbol', 'long_name', 'omim_id']).to_dict(orient='index')

mesh = pd.read_csv(snakemake.input[5], sep='|', header=None, index_col=None, names=['onto_id', 'name'])
mesh['onto_id'] = 'MESH:' + mesh['onto_id']
mesh.index = mesh['onto_id']
mesh = mesh.to_dict(orient='index')

combined_names = {**bto, **cl, **cvcl, **chebi}
assert len(combined_names) == len(bto) + len(cl) + len(cvcl) + len(chebi)

def get_name(onto_id):
    if onto_id in combined_names:
        return combined_names[onto_id].name
    if onto_id in mesh:
        return mesh[onto_id]['name']
    if onto_id in hgnc:
        return hgnc[onto_id]['symbol']
    return None

print("BEFORE\n", df_tags['ontology'].value_counts())
print("Total: ", df_tags.shape[0])

df_tags['name'] = df_tags['onto_id'].map(get_name)
unmapped = df_tags.loc[df_tags['name'].isna(), :]
print("\n\nUnmapped: ", unmapped)
unmapped.to_csv(snakemake.output[1], sep='|', header=None, index=None)

df_tags = df_tags.dropna()
print("\n\nAFTER\n", df_tags['ontology'].value_counts())
print("Total: ", df_tags.shape[0])
df_tags[['gse', 'type', 'ontology', 'ontology_id', 'name']].to_csv(snakemake.output[0], sep='|', index=None)
