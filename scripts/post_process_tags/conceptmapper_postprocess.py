import pandas as pd

bto_df = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col=None, names=['gse', 'field', 'ontology', 'ontology_id', 'keyword', 'span_begin', 'span_end'], dtype=str)

bto_refined = pd.read_csv(snakemake.input[1], sep='|', header=None, index_col=None, names=['onto_id', 'type', 'name'], dtype=str)
bto_refined['ontology'] = bto_refined['onto_id'].str.split(':').str[0]
bto_refined['ontology_id'] = bto_refined['onto_id'].str.split(':').str[1]

bto_df = bto_df.merge(bto_refined, on = ['ontology', 'ontology_id'])[['gse', 'field', 'ontology', 'ontology_id', 'keyword', 'span_begin', 'span_end', 'type']]

cl_df = pd.read_csv(snakemake.input[2], sep='|', header = None, index_col=None, names=['gse', 'field', 'ontology', 'ontology_id', 'keyword', 'span_begin', 'span_end'], dtype=str)
cl_df['type'] = 'celltype'

output = bto_df.append(cl_df).drop_duplicates()

print("Output df: \n", output)

output.to_csv(snakemake.output[0], sep='|', header = None, index = None)
