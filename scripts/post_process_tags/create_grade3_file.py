import pandas as pd
import sys, os
sys.path.append(os.path.join(os.getcwd(), 'scripts/parse_ontologies/'))
from parse_obo import parse_obo

df = pd.read_csv(snakemake.input[0], sep='|', header=None, index_col=None, names=['gsm', 'ontology', 'ontology_id', 'type'])

# exclude MiRNA, pathway etc
df = df[df['type'].isin(["gsm","cellline","celltype","disease","drug","gene","tissue"])]

bto, bto_roots = parse_obo(snakemake.input[1])
cl, cl_roots = parse_obo(snakemake.input[2])
cvcl, cvcl_roots = parse_obo(snakemake.input[3])
chebi, chebi_roots = parse_obo(snakemake.input[4], is_chebi=True)

hgnc = pd.read_csv(snakemake.input[6], sep='\t', names=['symbol', 'long_name', 'omim_id']).to_dict(orient='index')

mesh = pd.read_csv(snakemake.input[5], sep='|', header=None, index_col=None, names=['onto_id', 'name'])
mesh['onto_id'] = 'MESH:' + mesh['onto_id']
mesh.index = mesh['onto_id']
mesh = mesh.to_dict(orient='index')

combined_names = {**bto, **cl, **cvcl, **chebi}
assert len(combined_names) == len(bto) + len(cl) + len(cvcl) + len(chebi)

def get_name(onto_id):
    if onto_id in combined_names:
        return combined_names[onto_id].name
    if onto_id in mesh:
        return mesh[onto_id]['name']
    if onto_id in hgnc:
        return hgnc[onto_id]['symbol']
    return None

df['onto_id'] = df['ontology'] + ':' + df['ontology_id']

print("BEFORE\n", df['ontology'].value_counts())
print("Total: ", df.shape[0])

df['name'] = df['onto_id'].map(get_name)

unmapped = df.loc[df['name'].isna(), :]
print("\n\nUnmapped: ", unmapped)
df = df.dropna()

# Only keep BTO tissue, discard BTO cell types and cell lines
df = df.loc[((df['ontology'] == 'BTO') & (df['type'] == 'tissue')) | (df['ontology'] != 'BTO')]

df_names = df.groupby(['gsm', 'type'])['name'].first().reset_index()

pd.pivot(df_names, values='name', columns='type', index='gsm').to_csv(snakemake.output[0])
