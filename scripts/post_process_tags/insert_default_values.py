import pandas as pd
import json

# ----------- SAMPLE TAGS ---------------
df_sample_tags = pd.read_csv(snakemake.input[0])

# fill NaNs in disease column with 'Normal', all else with 'none'
df_sample_tags['disease'] = df_sample_tags['disease'].fillna('Normal')
df_sample_tags = df_sample_tags.fillna('none')

df_sample_tags = df_sample_tags.rename(columns={
    'gsm': 'geo_accession',
    'cellline': 'kw_curated_cell_line',
    'celltype': 'kw_curated_cell_type',
    'disease': 'kw_curated_disease',
    'drug': 'kw_curated_drug',
    'gene': 'kw_curated_gene',
    'tissue': 'kw_curated_tissue'
    })

df_sample_tags['kw_curated_genetic_mod_type'] = 'none'
df_sample_tags['kw_curated_modified_gene'] = 'none'

df_sample_tags.to_csv(snakemake.output[0], index=None)


# ----------- STUDY TAGS ---------------
df_tags = pd.read_csv(snakemake.input[1], sep='|')

# Removing BTO cell types
df_tags = df_tags[~((df_tags['ontology'] == 'BTO') & (df_tags['type'] != 'tissue'))].copy()

assert df_tags['type'].nunique() == 6

# Renaming
df_tags['type'] = df_tags['type'].map({
    'celltype': 'kw_cell_type',
    'cellline': 'kw_cell_line',
    'disease': 'disease',
    'tissue': 'tissue',
    'gene': 'kw_gene',
    'drug': 'kw_drug'
    })

tags_json = df_tags.groupby('gse').apply(
        lambda x: x.groupby('type')['name'].apply(list).to_dict()
        ).to_dict()

# Default values
default_dict = {
        'kw_cell_type': ['None'],
        'kw_cell_line': ['None'],
        'disease': ['Normal'],
        'tissue': ['None'],
        'kw_gene': ['None'],
        'kw_drug': ['None']
        }

# Apply default values
for gse in tags_json.keys():
    # When using this syntax, second dictionary overwrites first one
    tags_json[gse] = { **default_dict, **tags_json[gse] }

with open(snakemake.output[1], 'w') as f:
    json.dump(tags_json, f, indent = 2)
