import pandas as pd
from itertools import chain
import sys, os
sys.path.append(os.path.join(os.getcwd(), 'scripts/parse_ontologies/'))

from parse_obo import parse_obo
from mesh import parse_mesh_json

df = pd.read_csv(snakemake.input[0], sep='|', header=None, index_col=None, names=['gsm', 'keyword', 'ontology', 'ontology_id', 'type'])

print("Reading BTO, CL and MESH")
bto, bto_roots = parse_obo(snakemake.input[1])
cl, cl_roots = parse_obo(snakemake.input[2])
mesh, mesh_roots = parse_mesh_json(snakemake.input[3])

def get_adjacency_set(root):
    # tree is a set of all nodes in the tree corresponding to this 'root'
    tree = set([root.ontology_id])
    
    if not root.descendants:
        return set(), tree
        
    adjacencies = set()
    for _, child_oid in root.descendants:
        
        child_adj, subtree = get_adjacency_set(child_oid)
        for node_oid in subtree:
            adjacencies.add((root.ontology_id, node_oid))
        
        tree = tree.union(subtree)
        adjacencies = adjacencies.union(child_adj)
    return adjacencies, tree

print("Creating list of (parent, indirect child) pairs")
adj = set()
for root in chain(bto_roots, cl_roots, mesh_roots):
   adj = adj.union(get_adjacency_set(root)[0])

def filter_non_specific(oids): 
    """
    Returns the most-specific ontology ids in the input
    """
    filtered = set(oids) 
    for i in range(len(oids)): 
        for j in range(len(oids)): 
            if (oids[i], oids[j]) in adj: 
                filtered.discard(oids[i]) 
    return list(filtered)

print("Total tags: ", df.shape[0])

df = df.groupby(['gsm', 'type']).apply(lambda x: filter_non_specific((x['ontology']+':'+x['ontology_id']).values)).explode().reset_index().rename(columns={0:'onto_id'})

print("After filtering non-specific: ", df.shape[0])

# This may not always work as expected (especially for CUI-less)
# lucking we don't have CUI-less samples tags
df['ontology'] = df['onto_id'].str.split(':').str[0]
df['ontology_id'] = df['onto_id'].str.split(':').str[1]

df[['gsm', 'ontology', 'ontology_id', 'type']].sort_values(['gsm', 'type', 'ontology', 'ontology_id']).to_csv(snakemake.output[0], sep='|', header = None, index = None)
