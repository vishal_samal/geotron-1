import pandas as pd
from itertools import chain
import sys, os
sys.path.append(os.path.join(os.getcwd(), 'scripts/parse_ontologies/'))

from parse_obo import parse_obo
from mesh import parse_mesh_json

def read_csv(filename, names):
    return pd.read_csv(filename, sep='|', header=None, index_col=None, names=names, dtype=str)

print("Reading BTO, CL and MESH")
bto, bto_roots = parse_obo(snakemake.input[5])
cl, cl_roots = parse_obo(snakemake.input[6])
mesh, mesh_roots = parse_mesh_json(snakemake.input[7])

def get_adjacency_set(root):
    # tree is a set of all nodes in the tree corresponding to this 'root'
    tree = set([root.ontology_id])
    
    if not root.descendants:
        return set(), tree
        
    adjacencies = set()
    for _, child_oid in root.descendants:
        
        child_adj, subtree = get_adjacency_set(child_oid)
        for node_oid in subtree:
            adjacencies.add((root.ontology_id, node_oid))
        
        tree = tree.union(subtree)
        adjacencies = adjacencies.union(child_adj)
    return adjacencies, tree

print("Creating list of (parent, indirect child) pairs")
adj = set()
for root in chain(bto_roots, cl_roots, mesh_roots):
   adj = adj.union(get_adjacency_set(root)[0])

def filter_non_specific(oids): 
    """
    Returns the most-specific ontology ids in the input
    """
    filtered = set(oids) 
    for i in range(len(oids)): 
        for j in range(len(oids)): 
            if (oids[i], oids[j]) in adj: 
                filtered.discard(oids[i]) 
    return list(filtered)

cm_tags = read_csv(snakemake.input[0], ['gse', 'field', 'ontology', 'ontology_id', 'keyword', 'span_begin', 'span_end', 'type'])[['gse', 'ontology', 'ontology_id', 'type']].drop_duplicates()

bern_tags = read_csv(snakemake.input[1], ['gse', 'field', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end'])[['gse', 'ontology', 'ontology_id', 'type']].drop_duplicates()
#bern_tags = bern_tags.loc[~bern_tags['ontology'].str.startswith('CUI'), :]
bern_pubmed_tags = read_csv(snakemake.input[2], ['gse', 'pubmed_id', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end'])[['gse', 'ontology', 'ontology_id', 'type']].drop_duplicates()

sample_tags_gsm = read_csv(snakemake.input[3], names=['gsm', 'keyword', 'ontology', 'ontology_id', 'type'])
gse_gsm = read_csv(snakemake.input[4], names=['gse', 'gsm', 'source_name_ch1'])[['gse', 'gsm']].drop_duplicates()
sample_tags = sample_tags_gsm.merge(gse_gsm, on = 'gsm')
assert sample_tags.shape[0] >= sample_tags_gsm.shape[0] # one gsm -> many gses
sample_tags = sample_tags[['gse', 'ontology', 'ontology_id', 'type']].drop_duplicates()

print("Study tags: ", bern_tags.shape[0])
print("Pubmed tags: ", bern_pubmed_tags.shape[0])
print("Sample tags: ", sample_tags.shape[0])
tags = cm_tags.append(bern_tags).append(bern_pubmed_tags).append(sample_tags).drop_duplicates()
print("Total tags: ", tags.shape[0])

print("Filtering out non-specific tags and CUI-less tags")
tags = tags.loc[~tags['ontology'].str.startswith('CUI'), :] # if these aren't filtered out, then it may also lead to a bug in the next line (e.g. if ontology_id = 'Ac-50:5 cell line'
output = tags.groupby(['gse']).apply(lambda x: filter_non_specific((x['ontology']+':'+x['ontology_id']).values)).explode().reset_index().rename(columns={0:'onto_id'})

# Remove CL:0000000 and BTO:0001239 (serum) just for the sake of it
output = output.loc[output['onto_id'] != 'CL:0000000', :]
output = output.loc[output['onto_id'] != 'BTO:0001239', :]
output = output.loc[output['onto_id'] != 'BTO:0005055', :]

output = pd.concat([output[['gse']], output['onto_id'].str.split(':', expand=True).rename(columns={0:'ontology', 1:'ontology_id'})], axis=1)

# 'type' info was lost when filtering out tags, getting it back
shape_before = output.shape[0]
output = output.merge(tags[['ontology', 'ontology_id', 'type']].drop_duplicates(), on = ['ontology', 'ontology_id'])[['gse', 'type', 'ontology', 'ontology_id']].sort_values(by=['gse', 'type'])
# sanity check
assert output.shape[0] == shape_before

output.to_csv(snakemake.output[0], sep='|', header=None, index=None)
print("After filtering non-specific: ", output.shape[0])
