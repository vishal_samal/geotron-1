import pandas as pd

print("Reading cellline tags")
df = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col = None, names = ['gsm', 'keyword', 'ontology', 'ontology_id', 'type', 'field'], dtype=str)
print("Combined df shape: ", df.shape)

# discard 'field' column
df = df[['gsm', 'keyword', 'ontology', 'ontology_id', 'type']]

print("Reading tissue tags")
df = df.append(pd.read_csv(snakemake.input[1], sep='|', header = None, index_col = None, names = ['gsm', 'keyword', 'ontology', 'ontology_id', 'type'], dtype=str))
print("Combined df shape: ", df.shape)

print("Dropping duplicates")
df.drop_duplicates(inplace = True)
print("Combined df shape: ", df.shape)

# keeping single instance of an entity
# epithelial cell, BTO:1234
# epithelial cells, BTO:1234 ---------> epithelial cells, BTO:1234
df = df.sort_values(['keyword']).drop_duplicates(['gsm', 'ontology', 'ontology_id', 'type'])

# dropping BTO entities if keyword matches with a CL entity
# epithelial cells, BTO:1234
# epithelial cells, CL:1234 ---------> epithelial cells, CL:1234
df = df.sort_values(['ontology'], ascending=False).drop_duplicates(['gsm', 'keyword'])

df_bern = pd.read_csv(snakemake.input[2], sep='|', header = None, index_col = None, names = ['gsm', 'keyword', 'ontology', 'ontology_id', 'type', 'field'], dtype=str)

df_bern = df_bern.append(pd.read_csv(snakemake.input[3], sep='|', header = None, index_col = None, names = ['gsm', 'keyword', 'ontology', 'ontology_id', 'type', 'field'], dtype=str))

# discard 'field' column
df_bern = df_bern[['gsm', 'keyword', 'ontology', 'ontology_id', 'type']]

df_bern = df_bern.sort_values(['keyword'], ascending=False).drop_duplicates(['gsm', 'ontology', 'ontology_id', 'type'])

# dropping based on (gsm, keyword, type) pair
# hepatocellular carcinoma|MESH|D006528|disease
# hepatocellular carcinoma|OMIM|114550|disease ----> hepatocellular carcinoma|OMIM|114550|disease
df_bern = df_bern.sort_values(['ontology'], ascending=False).drop_duplicates(['gsm', 'keyword', 'type'])

df = df.append(df_bern)

df.sort_values(['gsm', 'type', 'ontology', 'ontology_id']).to_csv(snakemake.output[0], sep='|', header = None, index = None)
