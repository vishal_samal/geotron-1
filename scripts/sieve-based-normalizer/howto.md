### To compile and run
In src directory
```
javac tool/*.java tool/sieves/*.java tool/util/*.java
java tool.Main ../ncbi-data/training/ ../ncbi-data/test/ ../ncbi-data/TERMINOLOGY.txt 10
```

### re-run it on keywords from study metadata + pubmed abstracts

```
.once pubmed_data
select pubmed_id, title || " " || abstract from pubmed_abstracts;

.once study_metadata
select gse, title, summary, overall_design from studies;

.once pubmed_keywords
select pubmed_id, keyword, span_begin, span_end from bern_pubmed_entities where type = "disease";


.once study_keywords
select gse, keyword, field, span_begin, span_end from bern_study_entities where type = "disease";
```

in the TERMINOLOGY.txt provided by authors, remove `611490||OSTEOPETROSIS,  AUTOSOMAL RECESSIVE 4|OPTB4|OSTEOPETROSIS, INFANTILE MALIGNANT 2`
it causes weird behaviour

```
rm ncbi-data/test/*
rm ncbi-data/output/*
python prepare_input.py 0
sed -i 's/|/||/g' ncbi-data/test/*.concept
sed -i 's/%/|/g' ncbi-data/test/*.concept
cd src && java tool.Main ./ ../ncbi-data/test/ ../ncbi-data/TERMINOLOGY.txt 10

rm ncbi-data/test/*
python prepare_input.py 1
sed -i 's/|/||/g' ncbi-data/test/*.concept
sed -i 's/%/|/g' ncbi-data/test/*.concept
touch ncbi-data/test/GSE111889.txt 
cd src && java tool.Main ./ ../ncbi-data/test/ ../ncbi-data/TERMINOLOGY.txt 10

cat ncbi-data/output/* | sed 's/||/|/g' > name_of_file
```
For pubmed
```
rm ncbi-data/test/*
rm ncbi-data/output/*
python prepare_pubmed_input.py 0
sed -i 's/|/||/g' ncbi-data/test/*.concept
sed -i 's/%/|/g' ncbi-data/test/*.concept
cd src && java tool.Main ./ ../ncbi-data/test/ ../ncbi-data/TERMINOLOGY.txt 10

rm ncbi-data/test/*
python prepare_pubmed_input.py 1
sed -i 's/|/||/g' ncbi-data/test/*.concept
sed -i 's/%/|/g' ncbi-data/test/*.concept
cd src && java tool.Main ./ ../ncbi-data/test/ ../ncbi-data/TERMINOLOGY.txt 10

cat ncbi-data/output/* | sed 's/||/|/g' > name_of_file
```
There will be problems with the output file that will due to problems with `pubmed_keywords` input file.
This includes `NaN` values, same rows where columns are not ordered correctly etc.
The `study_keywords` file is mostly ok except some `NaN` values, so its output should be fine

### format DOID.synonyms to DOID-TERMINOLOGY.txt

```
python format_doid.py
sed -i 's/%/||/' ncbi-data/DOID-TERMINOLOGY.txt
```
