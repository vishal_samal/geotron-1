# coding: utf-8
import pandas as pd
import sys

assert int(sys.argv[1]) in (0, 1)

print("Reading study_metadata")
df_meta = pd.read_csv('study_metadata', sep = '|', header = None, index_col = None, names = ['gse', 'title', 'summary', 'overall_design'])
df_keywords = pd.read_csv('study_keywords', sep = '|', header = None, index_col=None, names = ['gse', 'keyword', 'field', 'span_begin', 'span_end'])
print("Total keywords: ", df_keywords.shape[0])
df_offset_title = df_meta[['gse']].copy()
df_offset_title['offset'] = 0
df_offset_title['field'] = 'title'
df_offset_summary = df_meta[['gse']].copy()
df_offset_summary['offset'] = df_meta['title'].str.len()
df_offset_summary['field'] = 'summary'
df_offset_od = df_meta[['gse']].copy()
df_offset_od['offset'] = df_meta['title'].str.len() + df_meta['summary'].str.len()
df_offset_od['field'] = 'overall_design' 
df_offset = pd.concat([df_offset_title, df_offset_summary, df_offset_od]).sort_values(by = 'gse')
df_keywords = df_keywords.merge(df_offset).sort_values(by = 'gse')

df_keywords['span'] = (df_keywords['span_begin'] + df_keywords['offset']).astype(str) + '%' + (df_keywords['span_end']+df_keywords['offset']).astype(str)
df_keywords['id'] = df_keywords['gse'] 
df_keywords['type'] = df_keywords['field']
df_keywords['onto'] = 'D009369'

print("Total keywords after computing offsets: ", df_keywords.shape[0])

print("Writing *.concept files")
groups = df_keywords.groupby('gse')
for gse, df_group in groups:
    if int(gse[3:])%2 == int(sys.argv[1]):
        continue
    df_group[['id', 'span', 'type', 'keyword', 'onto']].to_csv('ncbi-data/test/' + gse + '.concept', sep = '|', header = None, index = None)
    
print("Writing *.txt files")
for i, row in df_meta.iterrows():
    gse = row['gse']
    if int(row['gse'][3:])%2 == int(sys.argv[1]):
        continue
    with open('ncbi-data/test/' + row['gse'] + '.txt', 'w') as f:
        f.write(str(row['title']) + '\n'*2 + str(row['summary']) + '\n' + str(row['overall_design']))
