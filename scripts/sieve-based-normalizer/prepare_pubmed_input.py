# coding: utf-8
import pandas as pd
import sys

assert int(sys.argv[1]) in (0, 1)

df_meta = pd.read_csv('pubmed_data', sep = '|', header = None, index_col = None, names = ['pmid', 'abstract'])

df_keywords = pd.read_csv('pubmed_keywords', sep = '|', header = None, index_col=None, names = ['pmid', 'keyword', 'span_begin', 'span_end'])
print("Total keywords: ", df_keywords.shape[0])

df_keywords['span'] = df_keywords['span_begin'].astype(str) + '%' + df_keywords['span_end'].astype(str)
df_keywords['id'] = df_keywords['pmid'] 
df_keywords['type'] = 'DISO'
df_keywords['onto'] = 'D009369'

print("Writing *.concept files")
groups = df_keywords.groupby('pmid')
for pmid, df_group in groups:
    if int(pmid)%2 == int(sys.argv[1]):
        continue
    df_group[['id', 'span', 'type', 'keyword', 'onto']].to_csv('ncbi-data/test/' + str(pmid) + '.concept', sep = '|', header = None, index = None)
    
print("Writing *.txt files")
for i, row in df_meta.iterrows():
    pmid = row['pmid']
    if int(pmid)%2 == int(sys.argv[1]):
        continue
    with open('ncbi-data/test/' + str(row['pmid']) + '.txt', 'w') as f:
        f.write(row['abstract'])
