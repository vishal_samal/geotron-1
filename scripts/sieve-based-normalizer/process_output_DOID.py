# coding: utf-8
import pandas as pd
df_meta = pd.read_csv('study_metadata', sep = '|', header = None, index_col = None, names = ['gse', 'title', 'summary', 'overall_design'])

df_keywords = pd.read_csv('study_keywords', sep = '|', header = None, index_col=None, names = ['gse', 'keyword', 'field', 'span_begin', 'span_end'])

df_offset_title = df_meta[['gse']].copy()
df_offset_title['offset'] = 0
df_offset_title['field'] = 'title'
df_offset_summary = df_meta[['gse']].copy()
df_offset_summary['offset'] = df_meta['title'].str.len()
df_offset_summary['field'] = 'summary'
df_offset_od = df_meta[['gse']].copy()
df_offset_od['offset'] = df_meta['title'].str.len() + df_meta['summary'].str.len()
df_offset_od['field'] = 'overall_design'
df_offset = pd.concat([df_offset_title, df_offset_summary, df_offset_od]).sort_values(by = 'gse')
df_keywords = df_keywords.merge(df_offset).sort_values(by = 'gse')

df_keywords['begin_offset'] = df_keywords['span_begin'] + df_keywords['offset']
df_keywords['end_offset'] = df_keywords['span_end'] + df_keywords['offset']
df_normalized = pd.read_csv('study_normalized_DOID', sep = '|', header = None, index_col=None, names = ['gse', 'begin_offset', 'end_offset', 'keyword_lower', 'ontology_id'])
df_output = df_keywords.merge(df_normalized, on = ['gse', 'begin_offset', 'end_offset'])[['gse', 'keyword', 'field', 'span_begin', 'span_end', 'ontology_id']]
df_output
df_output['ontology'] = df_output['ontology_id']
df_output
df_output.loc[df_output['ontology'] != "CUI-less", 'ontology'] = 'DOID'
df_output.loc[df_output['ontology'] == "CUI-less", 'ontology_id'] = df_output[df_output['ontology'] == "CUI-less"]['keyword']
df_output['ontology'].value_counts()
df_output[['gse', 'keyword', 'ontology', 'ontology_id', 'field', 'span_begin', 'span_end']].to_csv('study_normalized_processed_DOID', sep='|', header = None, index = None)
