# coding: utf-8
import pandas as pd
df_meta = pd.read_csv('pubmed_data', sep = '|', header = None, index_col = None, names = ['pmid', 'abstract'])
df_keywords = pd.read_csv('pubmed_keywords', sep = '|', header = None, index_col=None, names = ['pmid', 'keyword', 'span_begin', 'span_end'])

df_normalized = pd.read_csv('pubmed_normalized_MEDIC_v5', sep = '|', header = None, index_col=None, names = ['pmid', 'span_begin', 'span_end', 'keyword_lower', 'ontology_id'])

df_keywords = df_keywords[~df_keywords['span_begin'].isna()]
df_keywords = df_keywords[~(df_keywords['keyword'] == 'BERN')]

df_normalized = df_normalized[~(df_normalized['keyword_lower'] == 'bern')]
df_normalized = df_normalized[~df_normalized['span_begin'].isna()]

df_output = df_keywords[['pmid', 'span_begin', 'span_end', 'keyword']].sort_values(by=['pmid', 'span_begin', 'span_end', 'keyword']).reset_index(drop=True).copy()


df_output['ontology_id'] = df_normalized.sort_values(by=['pmid', 'span_begin', 'span_end', 'keyword_lower'])['ontology_id'].values.copy()

df_output = df_output[~df_output['keyword'].isna()]

print(df_output)

df_output['ontology'] = df_output['ontology_id']


df_output.loc[df_output['ontology'].str.contains('^[CD][^U]'), 'ontology'] = 'MESH'

# weird stuff
df_output.loc[df_output['ontology'] == 'C' , 'ontology'] = 'CUI-less'
df_output.loc[df_output['ontology'] == "CUI-less", 'ontology_id'] = df_output[df_output['ontology'] == "CUI-less"]['keyword']

df_output.loc[df_output['ontology'].str.contains('^\d'), 'ontology'] = 'OMIM'

print(df_output['ontology'].value_counts())
print(df_output['ontology_id'].value_counts())

df_output[['pmid', 'keyword', 'ontology', 'ontology_id', 'span_begin', 'span_end']].to_csv('pubmed_normalized_processed_MEDIC', sep='|', header=None, index=None)
