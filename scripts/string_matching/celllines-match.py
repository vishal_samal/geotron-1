# coding: utf-8
import pandas as pd

# input: parsed_characteristics
df = pd.read_csv(snakemake.input[0], sep = '|', names = ['gse', 'gpl', 'gsm', 'prop', 'value'])[['gsm', 'prop', 'value']]
df = df.fillna('')

df = df.loc[df['prop'].str.contains('cell', case=False), :]
df = df.loc[df['prop'].str.contains('line', case=False), :]

# if df is empty at this point, it means no 'cellline' property names were found in it. So just writing an empty dataframe
if len(df) == 0:
    print("Couldn't find cell line columns")
    pd.DataFrame().to_csv(snakemake.output[0], sep='|', header=None, index=None)
    exit()

# input: cellline.lexicon
with open(snakemake.input[1]) as f:
    syns = f.readlines()

# removing newline character
syns = [syn[:-1] for syn in syns]

# mapping from name to ontology_id
synonym_map = { syn.split('|')[1] : syn.split('|')[0] for syn in syns }

print("Ontology id for PANC-1: ", synonym_map['PANC-1'])

# def match(value):
#     for key in synonym_map.keys():
#         if len(key)>2 and value==value and key.lower() in value.lower():
#             return synonym_map[key]
#     return None

# doing exact matches
print("Doing exact matches")
first = df['value'].map(synonym_map)
second = (df['value'] + ' cell').map(synonym_map)

#third = df['value'].map(match)

(first.isna() & second.isna()).sum()

df = df[['gsm', 'prop', 'value']]

#df.loc[~third.isna(), 'onto'] = third.loc[~third.isna()].values.copy()

df.loc[~second.isna(), 'onto'] = second.loc[~second.isna()].values.copy()
df.loc[~first.isna(), 'onto'] = first.loc[~first.isna()].values.copy()
output = df.loc[~df['onto'].isna()].copy()

# BTO:1234 -> [BTO, 1234]
ids = output['onto'].str.split(':', expand = True)
output['ontology'] = ids[0]
output['ontology_id'] = ids[1]

print("preparing and writing output")
# { "BTO:0002552": "MCF-7" }
inverse_map = {value: key for key, value in synonym_map.items()}

output['keyword'] = output['onto'].map(inverse_map)

output['type'] = 'cellline'

output[['gsm', 'keyword', 'ontology', 'ontology_id', 'type', 'prop']].to_csv(snakemake.output[0], sep='|', header=None, index=None)
