import pandas as pd
from collections import defaultdict
import csv

print("Loading files and preprocessing input")

keywords = pd.read_csv(snakemake.input[0], sep='|', header = None, index_col=None, names=['gse', 'field', 'keyword', 'ontology', 'ontology_id', 'type', 'span_begin', 'span_end'], quoting=csv.QUOTE_NONE)

# Filter out CUI-less, remove duplicates
keywords = keywords.loc[keywords['ontology'] != "CUI-less", ['gse', 'keyword', 'ontology', 'ontology_id', 'type']].drop_duplicates()

# strip off gene knockout symbols and lowercase
keywords['keyword'] = keywords['keyword'].str.lower().str.replace('\-\/\-', '').str.replace('\+\/\+', '').str.replace('\+\/\-', '').str.replace('\-\/\+', '').str.strip()

keywords = keywords.drop_duplicates()

# removing 2 letter keywords if they contain a vowel
keywords = keywords[~( (keywords['keyword'].str.len() < 3) & (keywords['keyword'].str.contains('a|e|i|o|u')) & ~(keywords['keyword'].str.contains('[0-9]').astype(bool)) )]

# removing all 1 letter keywords
keywords = keywords[keywords['keyword'].str.len()>1]

gse_keyword = defaultdict(list)
for i, row in keywords.iterrows():
    gse_keyword[row['gse']].append((row['keyword'], row['ontology'], row['ontology_id'], row['type']))

def matched_entities():
    for chunk in pd.read_csv(snakemake.input[1], sep='|', header =None, index_col=None, names=['gse', 'gpl', 'gsm', 'prop', 'value'], chunksize=10000):
        for i, row in chunk.iterrows():
            for keyword, onto, onto_id, type_ in gse_keyword[row['gse']]:
                if row['value']==row['value'] and keyword in row['value'].lower():
                    yield {'gsm': row['gsm'], 'keyword': keyword, 'ontology': onto, 'ontology_id': onto_id, 'type': type_, 'field': row['prop']}

print("Matching entities")
output = pd.DataFrame(matched_entities())

output.to_csv(snakemake.output[0], sep='|', header=None, index=None)
