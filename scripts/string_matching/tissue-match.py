import re
import pandas as pd

print("Reading parsed characteristics")
df_char = pd.read_csv(snakemake.input[0], header = None, index_col=None, sep='|', names=['gse', 'gpl', 'gsm', 'prop', 'value'])

print("Reading source_name_ch1")
df_source = pd.read_csv(snakemake.input[1], sep='|', header=None, index_col=None, names=['gse', 'gsm', 'value'])

df_metadata = df_char[['gsm', 'value']].append(df_source[['gsm', 'value']])
print("Shape: ", df_metadata.shape)

df_metadata['value'] = df_metadata['value'].fillna('')


print("Reading lexicon")
df_lex = pd.read_csv(snakemake.input[2], header = None, index_col=None, sep='|', names=['onto', 'syn', 'is_primary'])
df_lex['ontology'] = df_lex['onto'].str.split(':').str[0]
df_lex['ontology_id'] = df_lex['onto'].str.split(':').str[1]

# BTO:0006076 BPH cell False
# BTO:0005400 BPH cell True -------> BTO:0005400 BPH cell True
df_lex.sort_values(by='is_primary', ascending=False).drop_duplicates(['ontology', 'syn'])
print("Shape: ", df_lex.shape)


df_concept_names = df_lex[df_lex['is_primary']][['onto', 'syn']].rename(columns={'syn': 'name'})

# Making sure there's a name for every concept
assert df_concept_names.shape[0] == df_lex['onto'].nunique()

# Creating two separate dictionaries for BTO and CL because they contain overlapping keywords
lex_cl, lex_bto = {}, {}
for i, row in df_lex.iterrows():
    if row['onto'][:2] == "CL":
        lex_cl[row['syn']] = row['onto']
    else:
        lex_bto[row['syn']] = row['onto']

class SimpleTagger():
    """
    This is alchemy
    """
    def __init__(self, lexicon):
        self.lex = {syn.lower(): onto for syn,onto in lexicon.items()}

    def _lookup(self, tokens, are_lower = False):
        phrase = ' '.join(tokens)
        if not phrase:
            return
        if phrase in self.lex:
            return self.lex[phrase]
        if phrase[-1] == 's' and phrase[:-1] in self.lex:
            return self.lex[phrase[:-1]]
        if not are_lower:
            return self._lookup(list(map(str.lower, tokens)), True)

    def _longest_match(self, tokens, n_max, matched = None):
        if n_max < 1:
            return []
        if matched is None:
            matched = [0]*len(tokens)
        tags = []
        for i in range(n_max, len(tokens)+1):
            # if there's already been a match
            if 1 in matched[i-n_max: i]:
                continue
            # if there's a match
            if self._lookup(tokens[i-n_max: i]):
                # set slice to 1
                matched[i-n_max: i] = [1]*n_max
                tags.append(self._lookup(tokens[i-n_max: i]))
        return tags + self._longest_match(tokens, n_max-1, matched)

    def match_value(self, val):
        phrases = map(lambda x: x.strip('+- '), (filter(lambda x: x!='', re.split('[^a-zA-Z0-9 +-]', val))))

        ontos = set()
        for phrase in phrases:
            ontos = ontos.union(self._longest_match(phrase.split(' '), 5))
        return ontos

print("Matching")
cl_tagger = SimpleTagger(lex_cl)
bto_tagger = SimpleTagger(lex_bto)

df_metadata['onto'] = df_metadata['value'].fillna('') \
        .apply(lambda x: list(cl_tagger.match_value(x).union(bto_tagger.match_value(x))))

df_tags = df_metadata[['gsm', 'onto']]
df_tags = df_tags.explode('onto').reset_index(drop=True).dropna().drop_duplicates()

df_tags = df_tags.merge(df_concept_names, on=['onto'])

bto_refined = pd.read_csv(snakemake.input[3], sep='|', header=None, names=['type', '_'], dtype=str)
bto_type = bto_refined['type'].to_dict()

# Only keep either CL tags or BTO tags that are present in the refined dictionary
df_tags['filter'] = df_tags['onto'].map(lambda o: o in bto_type.keys() or o[:2] == 'CL')
df_tags = df_tags.loc[df_tags['filter'], :][['gsm', 'onto', 'name']]

# Assign a type to each tag
df_tags['type'] = df_tags['onto'].map(lambda o: bto_type[o] if o[:3] == 'BTO' else 'celltype')

df_tags['ontology'] = df_tags['onto'].str.split(':').str[0]
df_tags['ontology_id'] = df_tags['onto'].str.split(':').str[1]

print("Writing results to ", snakemake.output[0])
print(df_tags)

df_tags[['gsm', 'name', 'ontology', 'ontology_id', 'type']].to_csv(snakemake.output[0], sep='|', header=None, index=None)
